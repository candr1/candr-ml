.PHONY: run build
OB := {
OP := (
CB := }
CP := )
build:
	sudo docker build -t 'candr-ml:latest' .
run: build
	script logfile.log -c 'sudo docker run -m $$$(OP)grep MemTotal /proc/meminfo | awk '"'"'$(OB)print $$2" * 3/4"$(CB)'"'"' | bc$(CP)k -it --env-file env.list -v /home/josh/Projects/candr/candr-ml/container_tmp/:/tmp/ -v /home/josh/Projects/candr/candr-ml/ssh_key:/root/.ssh/ --gpus=all --runtime nvidia candr-ml:latest ./ssh-tunnel-entrypoint.sh'
debug: build
	script logfile.log -c 'sudo docker run -m $$$(OP)grep MemTotal /proc/meminfo | awk '"'"'$(OB)print $$2" * 3/4"$(CB)'"'"' | bc$(CP)k -it --env-file env.list -v /home/josh/Projects/candr/candr-ml/container_tmp/:/tmp/ -v /home/josh/Projects/candr/candr-ml/ssh_key:/root/.ssh/ --gpus=all --runtime nvidia candr-ml:latest ./ssh-tunnel-entrypoint.sh -d'
profile: build
	script logfile.log -c 'sudo docker run -m $$$(OP)grep MemTotal /proc/meminfo | awk '"'"'$(OB)print $$2" * 3/4"$(CB)'"'"' | bc$(CP)k -it --env-file env.list -v /home/josh/Projects/candr/candr-ml/container_tmp/:/tmp/ -v /home/josh/Projects/candr/candr-ml/ssh_key:/root/.ssh/ --gpus=all --runtime nvidia candr-ml:latest ./ssh-tunnel-entrypoint.sh -m'
