#FROM registry.gitlab.com/candr1/candr-base-dockerfiles/candr-ml-base-dockerfile/candr-ml-base-dockerfile:latest
FROM candr-ml-base-dockerfile:latest
RUN mkdir -p /app
COPY src/ /app
WORKDIR /app
CMD python3 ./app.py
