#!/bin/bash
date
echo "Starting SSH"
autossh -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -i /root/.ssh/ssh-login-key -f -N -L 127.0.0.1:15432:localhost:5432 candr@lynx.mythic-beasts.com
while [ -z "$(ps x | grep "/usr/bin/ssh" | grep -v grep)" ]; do
	echo "Waiting for SSH to begin"
	sleep 1
done
echo "SSH started"
if [ "$1" = '-d' ]; then
	echo "Debug mode"
	python3 -m pdb app.py
elif [ "$1" = '-m' ]; then
	echo "Memory profile"
	python3 -m cProfile -o /tmp/app.pstats app.py
else
	python3 app.py
fi
date
