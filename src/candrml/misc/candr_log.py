import multiprocessing
import logging
import inspect
def candr_log(*args, instance=None):
    if not any(True for frame in inspect.stack() if frame[1].endswith('pdb.py')):
        return
    proc = multiprocessing.current_process()
    proc_str = ''
    instance_str = ''
    if proc._parent_pid:
        proc_str = '(' + str(proc.pid) + ')'
    if instance is not None:
        if isinstance(instance, type):
            instance_str = instance.__name__
        else:
            instance_str = instance.__class__.__name__
    preamble = " ".join([instance_str, proc_str]).strip()
    if len(preamble) > 0:
        preamble += ": "
    logged = preamble + ", ".join([str(a) for a in args])
    logging.warning(logged)
    return logged
