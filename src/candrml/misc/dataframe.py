import sqlite3
import tempfile
import pickle
import os
import gc
from pympler import tracker
import multiprocessing
from tqdm import tqdm
from more_itertools import partition
from . import duplex_queue
from .candr_log import candr_log

class DataFrame:
    def __init__(self, dbfile=None, name=None, lock=None, persist=False):
        if not dbfile:
            tmpfile = tempfile.NamedTemporaryFile()
            dbfile = tmpfile.name
            del tmpfile
        self._dbfile = dbfile
        self._persist = persist
        self._name = name
        self._conn = sqlite3.connect(self._dbfile, check_same_thread=False)
        self._cur = self._conn.cursor()
        self._cur.execute('''PRAGMA synchronous = OFF;''')
        self._cur.execute('''PRAGMA journal_mode = WAL;''')
        self._cur.execute('''CREATE TABLE IF NOT EXISTS data (id INTEGER PRIMARY KEY, data BLOB, length INTEGER);''')
        self._conn.commit()
        self._commit = True
        if lock:
            self._commit_lock = lock
        else:
            self._commit_lock = multiprocessing.Lock()
    def set_persist(self, persist=True):
        self._persist = persist
    def start_batch(self):
        self._commit = False
    def end_batch(self):
        self._commit = True
        self._conn.commit()
    def add(self, val):
        s = pickle.dumps(val, protocol=pickle.HIGHEST_PROTOCOL)
        self._commit_lock.acquire()
        self._cur.execute('''INSERT INTO data (data, length) VALUES(?, ?);''', (s,len(s)))
        if self._commit:
            self._conn.commit()
        self._commit_lock.release()
        del s
    def addmany(self, vals):
        s = []
        for val in vals:
            pick = pickle.dumps(val, protocol=pickle.HIGHEST_PROTOCOL)
            s.append((pick, len(pick)))
        self._commit_lock.acquire()
        self._cur.executemany('''INSERT INTO data (data, length) VALUES(?, ?);''', s)
        if self._commit:
            self._conn.commit()
        self._commit_lock.release()
        del s
    def set(self, key, val):
        s = pickle.dumps(val, protocol=pickle.HIGHEST_PROTOCOL)
        self._commit_lock.acquire()
        self._cur.execute('''REPLACE INTO data (id, data, length) VALUES(?, ?, ?);''',
                (key, s, len(s)))
        if self._commit:
            self._conn.commit()
        self._commit_lock.release()
        del s
    def get(self, key, use_lock=True):
        if use_lock:
            self._commit_lock.acquire()
        self._cur.execute('''SELECT data FROM data WHERE id = ?''', (key,))
        ret = self._cur.fetchone()
        if use_lock:
            self._commit_lock.release()
        if not ret:
            return None
        s = ret[0]
        return pickle.loads(s)
    def all_ids(self):
        self._commit_lock.acquire()
        self._cur.execute('''SELECT COUNT(*) FROM data;''')
        nrows = self._cur.fetchone()[0]
        self._cur.execute('''SELECT id FROM data;''')
        r = [r[0] for r in tqdm(self._cur, total=nrows, desc="Fetching all IDs from dataframe")]
        self._commit_lock.release()
        return r
    def __del__(self):
        self._conn.close()
        if self._dbfile and os.path.exists(self._dbfile) and self._persist is False:
            os.remove(self._dbfile)
class DataFrameDuplexQueueConsumer(duplex_queue.AbstractDuplexQueueConsumer):
    def __init__(self, duplexqueue, dataframe, label=None, use_lock=True):
        super(DataFrameDuplexQueueConsumer, self).__init__(duplexqueue, label)
        self._dataframe = dataframe
        self._kwargs['use_lock'] = use_lock
    def consume(self, inp, use_lock=True):
        candr_log("getting " + str(inp), instance=self)
        out = self._dataframe.get(inp, use_lock=use_lock)
        del inp
        return out
class DataFrameQueueConsumer(multiprocessing.Process):
    def __init__(self, dataframe, queue, batch_size=32, label=None):
        super(DataFrameQueueConsumer, self).__init__()
        self._dataframe = dataframe
        self._queue = queue
        self.batch_size = batch_size
        self._label = str(label) if label is not None else ""
        self._tracker = tracker.SummaryTracker()
    def run(self):
        batch = []
        items = self._queue.get()
        while items is not None:
            pred = lambda a: type(a) is tuple and len(a) == 2
            gitems, setme = partition(pred, items)
            for a in setme:
                try:
                    self._dataframe.set(a[0], a[1])
                except KeyError:
                    candr_log("Tried to set dataframe using: " + str(a),
                            instance=self)
                    raise
            items = list(gitems)
            need = self.batch_size - len(batch)
            itemlen = len(items)
            #print("[" + self._label + "] Consumer received " + str(itemlen) + " items")
            pre_add_batches = []
            while need < 0:
                pre_add_batches.append(batch[:self.batch_size])
                batch = batch[self.batch_size:]
                need = self.batch_size - len(batch)
            if len(pre_add_batches) > 0:
                self._dataframe.addmany(pre_add_batches)
            del pre_add_batches
            if itemlen < need:
                #print("[" + self._label + "] Consumed items, didn't make full batch")
                batch += items
            else:
                #print("[" + self._label + "] Consumed items with full batch")
                batch += items[:need]
                self._dataframe.add(batch)
                batch = items[need:]
            del items
            gc.collect()
            #print(self.__class__.__name__ + " " +
            #        str(multiprocessing.current_process().pid) + " summary:\n" +
            #        "\n".join(self._tracker.format_diff()))
            items = self._queue.get()
        # Chain to close other consumers, if present
        self._queue.put(None)
        if len(batch) > 0:
            # Add the rest in batches
            self._dataframe.addmany([batch[i:i+self.batch_size] for i in
                range(0, len(batch), self.batch_size)])
