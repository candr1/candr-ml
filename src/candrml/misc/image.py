from PIL import Image
from filestore import Filestore
import requests
import sys
import os
import io
from url_normalize import url_normalize
import numpy as np

class CloudImage:
    def __init__(self, imagestore, cache_dir=None):
        self._filestore = Filestore(overwrite=True)
        self._filestore.clean_up()
        self._imagestore = imagestore
        self._cache_dir = cache_dir
    def __del__(self):
        try:
            self._filestore.clean_up()
        except:
            pass
    def _in_store(self, key):
        try:
            ret = key in self._filestore
            return ret
        except KeyError:
            return False
    def get_image(self, filename):
        txt = None
        try:
            if self._cache_dir:
                cache_path = os.path.realpath(self._cache_dir + '/' + filename)
                #sys.stderr.write('[CloudImage] Reading from data cache: ' + cache_path + '\n')
                with open(cache_path, 'rb') as fp:
                    txt = fp.read()
            else:
                raise Exception
        except Exception:
            if self._in_store(filename):
                txt = self._filestore[filename]
            else:
                sys.stderr.write("[CloudImage] Fetching '" + filename + "' from server.\n")
                resp = requests.get(url_normalize(self._imagestore + '/' + filename))
                if not resp.ok:
                    raise Exception("Could not download image: " + filename + ".  Response was: " + resp.status_code + " " + resp.reason)
                self._filestore[filename] = resp.text
                txt = resp.content
                sys.stderr.write("[CloudImage] Fetched " + str(len(txt)) + " bytes.\n")
        im = None
        with io.BytesIO(txt) as f:
            im = Image.open(f).copy()
        return im

def export_image(arr, fname, export_dir='/tmp/debug-images/', skip=False):
    if skip:
        return
    floatarr = arr.astype('float')
    normalized = (255*(floatarr - np.min(floatarr))/np.ptp(floatarr)).astype(np.uint8)
    im = Image.fromarray(normalized)
    realdir = os.path.realpath(export_dir)
    if not os.path.exists(realdir):
        os.makedirs(realdir)
    realfname = os.path.realpath(realdir + '/' + fname)
    im.save(realfname, format='png')
    print("Successfully exported image to '" + realfname + "'", file=sys.stderr)
def otsu(gray):
    """Do otsu thresholding on an array.

        Parameters:
            gray (2D numpy array): Array to be thresholded

        Returns:
            otsu (2D numpy array): Thresholded array
    """
    pixel_number = gray.shape[0] * gray.shape[1]
    mean_weight = 1.0/pixel_number
    his, bins = np.histogram(gray, np.arange(0,257))
    final_thresh = -1
    final_value = -1
    intensity_arr = np.arange(256)
    for t in bins[1:-1]:
        pcb = np.sum(his[:t])
        pcf = np.sum(his[t:])
        Wb = pcb * mean_weight
        Wf = pcf * mean_weight
        with np.errstate(divide='ignore', invalid='ignore'):
            mub = np.sum(intensity_arr[:t]*his[:t]) / float(pcb)
            muf = np.sum(intensity_arr[t:]*his[t:]) / float(pcf)
        value = Wb * Wf * (mub - muf) ** 2
        if value > final_value:
            final_thresh = t
            final_value = value
    final_img = gray.copy()
    final_img[gray > final_thresh] = 255
    final_img[gray <= final_thresh] = 0
    return final_img
