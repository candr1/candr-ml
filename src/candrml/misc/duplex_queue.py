import multiprocessing
import queue
import uuid
from .candr_log import candr_log
class DuplexQueue():
    def __init__(self, manager, size=0):
        # Initialise the queues and events
        self._in_q = manager.Queue(size)
        self._out_top = manager.Queue(1)
        self._out_top_lock = manager.Lock()
        self._out_q = manager.Queue(size)
        self._event = manager.Event()
    # These functions to be called by a getter
    def put_in(self, val):
        # Add a value to be processed
        uniqid = uuid.uuid4().int
        self._in_q.put((uniqid, val))
        return uniqid
    def wait_event(self):
        # Blocking call to wait for a result
        return self._event.wait()
    def set_event(self):
        # Wake waiting threads
        return self._event.set()
    def clear_event(self):
        # Reset event
        return self._event.clear()
    def get_out(self, val):
        # Retrieve a value. Returns a tuple. The first value is a boolean
        # indicating if the value was ready, the second value is that value, if
        # done, None otherwise.
        self._sort_top()
        try:
            candr_log("Getting out. Waiting for lock", instance=self)
            with self._out_top_lock:
                candr_log("Gained lock", instance=self)
                # Peek the out_top
                peek = self._out_top.get(False)
                if peek[0] == val:
                    candr_log("It's there!", instance=self)
                    return (True, peek[1])
                candr_log("It's not. Item was for " + str(peek[0]),
                        instance=self)
                self._out_top.put(peek)
                return (False, None)
        except queue.Empty:
            # Sort out out_top
            candr_log("Nothing in top", instance=self)
            if self._sort_top():
                return self.get_out(val)
            # Queue could be entirely empty
            if self._out_q.empty():
                candr_log("Nothing in queue at all", instance=self)
                return (False, None)
            candr_log("Recursing")
            return self.get_out(val)
    def _sort_top(self):
        candr_log("Sort top. Waiting for lock", instance=self)
        do_event = False
        with self._out_top_lock:
            candr_log("Lock gained", instance=self)
            if self._out_top.empty() and not self._out_q.empty():
                candr_log("Putting item in top")
                self._out_top.put(self._out_q.get())
                do_event = True
        if do_event:
            self.set_event()
        return do_event
    def poison(self):
        # Putting 'None' into the queue should make the consumer exit
        self._in_q.put(None)
    # These functions called by a consumer to process the queue
    def get_in(self):
        # Get the next item to be processed (blocking)
        ret = self._in_q.get()
        return ret
    def set_out(self, key, val):
        candr_log("setting " + str(key) + " value, then setting event",
                instance=self)
        # Set the dict value to output
        self._sort_top()
        self._out_q.put((key, val))
        self._sort_top()
        # Set the event to wake getter processes up to check if it's their value
        self.set_event()
class DuplexQueueGetter():
    def __init__(self, duplexqueue):
        self._duplexqueue = duplexqueue
    def get(self, inp):
        candr_log("get " + str(inp), instance=self)
        # This is a blocking call to get an item from a slow function
        # Put the value in
        uniqid = self._duplexqueue.put_in(inp)
        # Every time the event is fired, a new value is ready
        while self._duplexqueue.wait_event():
            # Reset the event (others may be waiting)
            self._duplexqueue.clear_event()
            # Check if our value is done
            done = self._duplexqueue.get_out(uniqid)
            if done[0]:
                candr_log("got " + str(inp), instance=self)
                return done[1]
            else:
                candr_log("was waiting for " + str(inp) + " but event was not" +
                    " for this. Waiting for event", instance=self)
        # This should never happen as wait_event() should always return True
        raise Exception("Waiting for queue timed out")
class AbstractDuplexQueueConsumer(multiprocessing.Process):
    def __init__(self, queue, label=None):
        super(AbstractDuplexQueueConsumer, self).__init__()
        self._queue = queue
        self._label = str(label) if label is not None else ""
        # args and kwargs are passed to consume() each time
        self._args = []
        self._kwargs = {}
    def consume(self, *args, **kwargs):
        candr_log("consume", instance=self)
        # Function to be implemented in consumers
        return False
    def run(self):
        candr_log("run", instance=self)
        # Wait for the next time
        next_in = self._queue.get_in()
        # If it is 'None' then the queue has been poisoned, and we should exit
        while next_in is not None:
            candr_log("received item", instance=self)
            # Queue items are of the form (uniqid, value)
            uniqid = next_in[0]
            val = next_in[1]
            # Call consume() on the value to do our processing, and set it in
            # the output queue
            answer = self.consume(val, *(self._args), **(self._kwargs))
            candr_log("got response from consume()", instance=self)
            self._queue.set_out(uniqid, answer)
            del next_in
            del val
            del answer
            # Get the next item
            next_in = self._queue.get_in()
        candr_log("queue poisoned", instance=self)
        # continue the poisoning
        self._queue.poison()
class InOutQueue():
    def __init__(self, manager):
        self._in_queue = manager.Queue()
        self._out_queue = manager.Queue()
    def next_in(self):
        candr_log("next_in", instance=self)
        return self._in_queue.get()
    def next_out(self):
        candr_log("next_out", instance=self)
        return self._out_queue.get()
    def set_out(self, val):
        candr_log("set_out", val, instance=self)
        return self._out_queue.put(val)
    def poison_in(self):
        candr_log("poison_in", instance=self)
        self._in_queue.put(None)
    def poison_out(self):
        candr_log("poison_out", instance=self)
        self._out_queue.put(None)
    def fill_in(self, vals):
        candr_log("fill_in", vals, instance=self)
        _ = [self._in_queue.put(v) for v in vals]
