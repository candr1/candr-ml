import gnupg
import time
import requests
import base64
import threading
import json
from url_normalize import url_normalize
from datetime import datetime

class StatusUpdater:
    def __init__(self, host, private_key, password, pg_secrets, gpg_home='/root', dummy=False, timeout=5):
        self._host = host
        self._password = password
        self._pending_status = None
        self._updating = False
        self._last_updated = None
        self._timeout = timeout
        self._status_update_lock = threading.Lock()
        if not ({"database", "host", "username", "password"} <= pg_secrets.keys()):
            raise Exception("pg_secrets did not contain all necessary data")
        self._pg_database = pg_secrets['database']
        self._pg_host = pg_secrets['host']
        self._pg_username = pg_secrets['username']
        self._pg_password = pg_secrets['password']
        self._gpg = gnupg.GPG(gnupghome=gpg_home)
        self._status_updater = self._do_update_status
        if dummy:
            self._status_updater = lambda: print("[Dummy status] " + json.dumps(self._pending_status))
        else:
            res = self._gpg.import_keys(private_key)
            self._gpg.trust_keys(res.fingerprints, 'TRUST_ULTIMATE')
        self._started = datetime.now()
    def update_status_async(self, status):
        diff = int((datetime.now() - self._started).total_seconds())
        if isinstance(status, dict):
            timed_status = {**status, 'elapsed': diff}
        else:
            timed_status = {'status': status, 'elapsed': diff}
        self._pending_status = timed_status
        thread = threading.Thread(target=self._status_updater, args=tuple())
        thread.start()
    def update_status(self, status):
        diff = int((datetime.now() - self._started).total_seconds())
        if isinstance(status, dict):
            timed_status = {**status, 'elapsed': diff}
        else:
            timed_status = {'status': status, 'elapsed': diff}
        self._pending_status = timed_status
        self._status_updater()
    def _do_update_status(self):
        status = self._pending_status
        print("Updating status with:", json.dumps(status))
        timenow = time.time()
        if not self._status_update_lock.acquire(blocking=False):
            # Currently updating
            print("Already updating")
            return status
        if self._last_updated is not None and timenow < (self._last_updated + self._timeout):
            # To soon
            print("Too soon")
            next_update = self._last_updated + self._timeout
            diff = next_update - timenow
            time.sleep(diff)
            try:
                self._status_update_lock.release()
            except:
                pass
            return self._do_update_status()
        try:
            challenge_req = requests.get(url=url_normalize('http://' + self._host + '/challenge'))
            if not challenge_req.ok:
                raise Exception("Error getting challenge:", challenge_req.status_code, challenge_req.text)
            challenge = challenge_req.json()
            if not 'challenge' in challenge:
                raise Exception("API didn't provide a challenge")
            message = base64.b64decode(challenge['challenge'])
            decrypted = self._gpg.decrypt(message, passphrase=self._password)
            if not decrypted.ok:
                raise Exception("Could not decrypt challenge from API")
            answer = decrypted.data.decode('utf-8')
            post_data = {
                "answer": answer,
                "challenge": challenge['challenge'],
                "status": status
            }
            update_req = requests.post(url=url_normalize('http://' + self._host + '/status'), json=post_data)
            if not update_req.ok:
                raise Exception("Error setting status:", update_req.status_code, update_req.text, json.dumps(post_data))
        except Exception as e:
            try:
                self._status_update_lock.release()
            except:
                pass
            raise e
        try:
            self._status_update_lock.release()
        except:
            pass
        self._last_updated = time.time()
        if status != self._pending_status:
            return self._do_update_status()
        return status
