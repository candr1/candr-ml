from tensorflow import keras

class FitCallback(keras.callbacks.Callback):
    def __init__(self, epoch_fun=lambda: None):
        self.__epoch = None
        self.__epoch_fun = epoch_fun
        super(FitCallback, self).__init__()
    def on_epoch_begin(self, epoch, logs=None):
        self.__epoch = epoch
        self.__epoch_fun(epoch)
