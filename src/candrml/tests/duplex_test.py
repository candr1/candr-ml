from ..misc import duplex_queue, candr_log
from ..misc.candr_log import candr_log
import multiprocessing
import random
import time

class Worker(duplex_queue.AbstractDuplexQueueConsumer):
    def consume(self, wid):
        candr_log("consume " + str(wid), instance=self)
        time.sleep(random.randint(5, 10))
        candr_log("consume " + str(wid) + " done", instance=self)
        return wid
class Producer(multiprocessing.Process):
    def __init__(self, dq, outq):
        super().__init__()
        self._getter = duplex_queue.DuplexQueueGetter(dq)
        self._outq = outq
    def run(self):
        ret = []
        for i in range(3):
            candr_log("producer getting " + str(i), instance=self)
            ans = self._getter.get(i)
            candr_log("got returned " + str(ans), instance=self)
            ret.append(ans)
            candr_log("current answers: " + str(ret), instance=self)
        candr_log("producer done!", instance=self)
        self._outq.put(ret)
# main
def test():
    man = multiprocessing.Manager()
    outq = multiprocessing.Queue()
    dq = duplex_queue.DuplexQueue(man, size=2)
    dqcons = [Worker(dq) for _ in range(2)]
    dqprds = [Producer(dq, outq) for _ in range(2)]
    candr_log("Starting workers")
    [dqcon.start() for dqcon in dqcons]
    candr_log("Starting producers")
    [dqprd.start() for dqprd in dqprds]
    candr_log("Waiting for producers")
    [dqprd.join() for dqprd in dqprds]
    candr_log("Poisoning queue")
    dq.poison()
    outq.put(None)
    candr_log("Waiting for workers")
    [dqcon.join() for dqcon in dqcons]
    candr_log("Getting results")
    print(list(iter(outq.get, None)))
    man.shutdown()
    candr_log("Done")
