import numpy as np
from . import generic
from ..misc import image
import operator
import cv2
import seaborn
import random
from PIL import Image, ImageDraw

class ItemDataGeneratorExecutor(generic.GenericDataGeneratorExecutor):
    def __init__(self, bucket, note_radius=5, window_size=40,
            supersample=5, out_w=1000, divisione_w=5, batch_size=32, step=4,
            training_data_queue_name='training_data', idx_names=[]):
        self.idx_names = idx_names
        self.idxs = {v: i for i, v in enumerate(self.idx_names)}
        self.note_radius = note_radius
        self.divisione_w = divisione_w
        super(ItemDataGeneratorExecutor, self).__init__(out_w, bucket, 
                supersample, window_size, batch_size, step,
                training_data_queue_name)
        self._pickle_me.extend(['idxs', 'note_radius', 'divisione_w'])
    def _initial_n(self):
        # Number of calls to _progress_list_complete_item in _looper
        return 9
    def _looper(self, staffid):
        #logging.warning(str(self._pid) + ': Entered looper for staffid %d', staffid)
        queue_name = self.training_data_queue_name
        max_h = self._max_h
        out_w = self.out_w
        idxs = self.idxs
        items = self._items
        supersample = self.supersample
        note_radius = self.note_radius
        window_size = self.window_size
        divisione_w = self.divisione_w
        step = self.step
        sample_ratio = [0] * len(idxs)
        shape = max_h, out_w
        # c_clef, f_clef, flats, sharps, notes
        # things to del after
        multi_mask = np.zeros((len(idxs), shape[0], shape[1]), dtype='float')
        mask = np.array([])
        npim = np.array([])
        true_mask = np.array([])
        decided_mask = np.array([])
        # main body
        if ('syllables' in idxs) and len(items['syllables'][staffid]) > 0:
            mask = np.zeros((shape[0] * supersample, shape[1] *
                supersample), dtype='uint8')
            for syllable in items['syllables'][staffid]:
                rect = (((syllable['left'] + syllable['width'] / 2) * supersample,
                    (syllable['top'] + syllable['height'] / 2) * supersample),
                    (syllable['width'] * supersample, syllable['height'] *
                    supersample), syllable['angle'])
                points = [np.int0(cv2.boxPoints(rect))]
                cv2.fillPoly(mask, pts=points, color=255)
            npim = np.array(cv2.resize(mask, (shape[1], shape[0]),
                interpolation=cv2.INTER_CUBIC)).reshape(shape).astype('float')
            with np.errstate(divide='ignore', invalid='ignore'):
                multi_mask[idxs['syllables']] = (npim - np.min(npim))/np.ptp(npim)
        self._progress_list_complete_item()
        if ('c_clefs' in idxs) and len(items['c_clefs'][staffid]) > 0:
            mask = np.zeros((shape[0] * supersample, shape[1] *
                supersample), dtype='uint8')
            for clef in items['c_clefs'][staffid]:
                rect = (((clef['left'] + clef['width'] / 2) * supersample,
                    (clef['top'] + clef['height'] / 2) * supersample),
                    (clef['width'] * supersample, clef['height'] *
                    supersample), clef['angle'])
                points = [np.int0(cv2.boxPoints(rect))]
                cv2.fillPoly(mask, pts=points, color=255)
            npim = np.array(cv2.resize(mask, (shape[1], shape[0]),
                interpolation=cv2.INTER_CUBIC)).reshape(shape).astype('float')
            with np.errstate(divide='ignore', invalid='ignore'):
                multi_mask[idxs['c_clefs']] = (npim - np.min(npim))/np.ptp(npim)
        self._progress_list_complete_item()
        if ('f_clefs' in idxs) and len(items['f_clefs'][staffid]) > 0:
            mask = np.zeros((shape[0] * supersample, shape[1] *
                supersample), dtype='uint8')
            for clef in items['f_clefs'][staffid]:
                rect = (((clef['left'] + clef['width'] / 2) *
                    supersample, (clef['top'] + clef['height'] / 2) *
                    supersample), (clef['width'] * supersample,
                    clef['height'] * supersample), clef['angle'])
                points = [np.int0(cv2.boxPoints(rect))]
                cv2.fillPoly(mask, pts=points, color=255)
            npim = np.array(cv2.resize(mask, (shape[1], shape[0]),
                interpolation=cv2.INTER_CUBIC)).reshape(shape).astype('float')
            with np.errstate(divide='ignore', invalid='ignore'):
                multi_mask[idxs['f_clefs']] = (npim - np.min(npim))/np.ptp(npim)
        self._progress_list_complete_item()
        if ('flats' in idxs) and len(items['flat_accidentals'][staffid]) > 0:
            mask = np.zeros((shape[0] * supersample, shape[1] *
                supersample), dtype='uint8')
            for flat in items['flat_accidentals'][staffid]:
                rect = (((flat['left'] + flat['width'] / 2) *
                    supersample, (flat['top'] + flat['height'] / 2) *
                    supersample), (flat['width'] * supersample,
                    flat['height'] * supersample), flat['angle'])
                points = [np.int0(cv2.boxPoints(rect))]
                cv2.fillPoly(mask, pts=points, color=255)
            npim = np.array(cv2.resize(mask, (shape[1], shape[0]),
                interpolation=cv2.INTER_CUBIC)).reshape(shape).astype('float')
            with np.errstate(divide='ignore', invalid='ignore'):
                multi_mask[idxs['flats']] = (npim - np.min(npim))/np.ptp(npim)
        self._progress_list_complete_item()
        if ('sharps' in idxs) and len(items['sharp_accidentals'][staffid]) > 0:
            mask = np.zeros((shape[0] * supersample, shape[1] *
                supersample), dtype='uint8')
            for sharp in items['sharp_accidentals'][staffid]:
                rect = (((sharp['left'] + sharp['width'] / 2) *
                    supersample, (sharp['top'] + sharp['height'] / 2) *
                    supersample), (sharp['width'] * supersample,
                    sharp['height'] * supersample), sharp['angle'])
                points = [np.int0(cv2.boxPoints(rect))]
                mask = cv2.fillPoly(mask, pts=points, color=255)
            npim = np.array(cv2.resize(mask, (shape[1], shape[0]),
                interpolation=cv2.INTER_CUBIC)).reshape(shape).astype('float')
            with np.errstate(divide='ignore', invalid='ignore'):
                multi_mask[idxs['sharps']] = (npim - np.min(npim))/np.ptp(npim)
        self._progress_list_complete_item()
        if ('notes' in idxs) and len(items['notes'][staffid]) > 0:
            mask = np.zeros((shape[0] * supersample, shape[1] *
                supersample), dtype='uint8')
            for note in items['notes'][staffid]:
                pts = (note[0] * supersample, note[1] *
                    supersample)
                mask = cv2.circle(mask, pts, note_radius * supersample, 255,
                    -1)
            npim = np.array(cv2.resize(mask, (shape[1], shape[0]),
                interpolation=cv2.INTER_CUBIC)).reshape(shape).astype('float')
            with np.errstate(divide='ignore', invalid='ignore'):
                multi_mask[idxs['notes']] = (npim - np.min(npim))/np.ptp(npim)
        self._progress_list_complete_item()
        if ('divisiones' in idxs) and len(items['divisiones'][staffid]) > 0:
            line_w = divisione_w * supersample
            mask = Image.new('L', tuple([shape[1] * supersample, shape[0] *
                supersample]))
            draw = ImageDraw.Draw(mask)
            for divisione in items['divisiones'][staffid]:
                line = [tuple([x * supersample for x in p]) for p in divisione]
                draw.line(line, fill=255, width=line_w)
            npim = np.array(mask.resize((shape[1], shape[0]),
                Image.ANTIALIAS)).reshape(shape).astype('float')
            with np.errstate(divide='ignore', invalid='ignore'):
                multi_mask[idxs['divisiones']] = (npim - np.min(npim))/np.ptp(npim)
        self._progress_list_complete_item()
        # Transpose to an array of shape y by x by feature (5)
        true_mask = np.transpose(multi_mask, (1, 2, 0))
        assert true_mask.shape == (shape[0], shape[1], len(idxs))
        # Make nothing a very small non-zero value, so it is chosen if
        # everything else is zero
        true_mask[:,:,idxs['nothing']] = np.nextafter(0, 1)
        decided_mask = np.zeros(true_mask.shape, dtype='uint8')
        # Make image colours
        pal = np.array(seaborn.color_palette(None, len(idxs)))
        colour_image = np.zeros((shape[0], shape[1], 3), dtype='uint8')
        for y in range(0, shape[0]):
            for x in range(0, shape[1]):
                brightest = np.argmax(true_mask[y,x])
                decided_mask[y,x,brightest] = 1
                col = pal[brightest]
                colour_image[y,x] = col * 255
        self._progress_list_complete_item()
        #image.export_image(colour_image, "item-mask-" + str(staffid) + ".png")
        # Do a moving window over each X and Y
        half_window = window_size // 2
        my_ret = []
        flips = ((), (0,))
        a_jit = random.randint(0, step)
        b_jit = random.randint(0, step)
        for x_start in range(a_jit, out_w - window_size, step):
            for y_start in range(b_jit, max_h - window_size, step):
                x_end = x_start + window_size
                y_end = y_start + window_size
                x_mid = x_start + half_window
                y_mid = y_start + half_window
                this_res = decided_mask[y_mid,x_mid]
                one_hot = this_res.tolist()
                # Flip data in y axis
                sample_ratio[np.argmax(this_res)] += len(flips)
                for flip in flips:
                    my_ret.append({
                        "sid": staffid,
                        "window": (x_start, y_start),
                        "dist": one_hot,
                        "flip": flip
                    })
        self._progress_list_complete_item()
        self._queue[queue_name].put(my_ret)
        del multi_mask
        del mask
        del npim
        del true_mask
        del decided_mask
        del my_ret
        #logging.warning(str(self._pid) + ': Exited looper for staffid %d', staffid)
        return sample_ratio
class ItemDataGenerator(generic.GenericDataGenerator):
    """Data generator for finding pixels"""
    def __init__(self, pg_conn, bucket, facsimile_set_id, out_w=1000, mul=0.02,
            window_size=40, batch_size=32, step=4, shuffle=True, supersample=5,
            note_radius=5, divisione_w=5, 
            training_data_queue_name='training_data'):
        """Initialise generator.

            Parameters:
                pg_conn (psycopg2 conn): Database connection
                bucket (CloudImage):     Google Cloud storage bucket to access images
                facsimile_set_id (int):  Database ID of the facsimile set to train for
                out_w (int):             Basic stave width
                mul (float):             Multiplier for border
                window_size (int):       Size of the moving window for training
                batch_size (int):        Number of samples to yield
                step (int):              How quickly the moving window moves
                shuffle (bool):          Randomise samples after each epoch
        """
        self.window_size = window_size
        self.step = step
        self.note_radius = note_radius
        self.supersample = supersample
        #self.idx_names = ["c_clefs", "f_clefs", "flats", "sharps", "notes", "syllables", "divisiones", "nothing"]
        #self.idx_names = ["c_clefs", "f_clefs", "flats", "sharps", "notes", "syllables", "nothing"]
        self.idx_names = ["notes", "divisiones", "nothing"]
        self.idxs = {v: i for i, v in enumerate(self.idx_names)}
        self.sample_ratio = [0] * len(self.idx_names)
        parallel_methods = ItemDataGeneratorExecutor(bucket, note_radius,
                window_size, supersample, out_w, divisione_w, batch_size, step,
                training_data_queue_name, self.idx_names)
        # Send this to the generic data generator
        super(ItemDataGenerator, self).__init__(pg_conn, parallel_methods,
            facsimile_set_id, out_w=out_w, mul=mul, batch_size=batch_size,
            shuffle=shuffle, name=self.__class__.__name__,
            training_data_queue_name=training_data_queue_name)
    def process(self, parallel_methods, transcribed=True):
        ret = super(ItemDataGenerator, self).process(parallel_methods,
                transcribed)
        if transcribed:
            for r in ret:
                lendiff = len(r) - len(self.sample_ratio)
                if lendiff > 0:
                    self.sample_ratio += [0] * lendiff
                self.sample_ratio = list(map(operator.add, self.sample_ratio, r))
        return
    def __getitem__(self, index):
        # Get one batch's worth of data
        idxs = self._new_range[index]
        inp = self._td_dataframe.get(idxs, use_lock=False)
        inplen = len(inp)
        X = np.empty((inplen, self.window_size, self.window_size, 3), dtype='float')
        Y = np.empty((inplen,len(self.idxs)), dtype='float')
        staff_data = {}
        for sid in set([record['sid'] for record in inp]):
            staff_data[sid] = self._stave_dataframe.get(sid, use_lock=False)
        for idx, record in enumerate(inp):
            sid = record['sid']
            window = record["window"]
            x_start = window[0]
            y_start = window[1]
            x_end = x_start + self.window_size
            y_end = y_start + self.window_size
            # Do the flip described
            seg = np.flip(staff_data[sid][y_start:y_end,x_start:x_end], record["flip"])
            X[idx] = seg.reshape(self.window_size, self.window_size, 3)
            Y[idx] = record["dist"]
        del inp
        del staff_data
        return X, Y
