import numpy as np
from tensorflow import keras
import json
import uuid
import tempfile
import random
import psutil
import time
import multiprocessing
from pqdm.processes import pqdm
from tqdm import tqdm
from PIL import Image
from ..misc import dataframe
from ..misc.candr_log import candr_log

class GenericDataGenerator(keras.utils.Sequence):
    """Generic class to prepare data for ML methods"""

    def __init__(self, pg_conn, parallel_methods, facsimile_set_id, out_w=1000,
            mul=0.02, batch_size=32, shuffle=True, name='',
            training_data_queue_name="training_data"):
        """Initialise generator.

            Parameters:
                pg_conn (psycopg2 conn):            Database connection
                bucket (CloudImage):                Google Cloud storage bucket to access images
                facsimile_set_id (int):             Database ID of the facsimile set to train for
                prelooper (function(self)):         Function run before processing training data
                out_w (int):                        Basic stave width
                mul (float):                        Multiplier for border
                batch_size (int):                   Number of samples to yield
                shuffle (bool):                     Randomise samples after each epoch
        """

        self.batch_size = batch_size
        self.name = name
        self.shuffle = shuffle
        self.mul = mul
        self.out_w = out_w
        self._conn = pg_conn
        self._cursor = self._conn.cursor()
        self._facsimile_set_id = facsimile_set_id
        self._sql_lock = multiprocessing.Lock()
        self._max_h = 0
        self.scaling = {}
        self._parallel_methods = parallel_methods
        # Number of processes to use
        num_procs = psutil.cpu_count()
        # Get all staves
        if not hasattr(self, '_rows'):
            staves = self._do_sql('''SELECT systems.id, staves.id, staves.transcribed, staves.last_predicted, facsimiles.image, systemwarps.nodes AS systemnodes, staffwarps.nodes AS staffnodes FROM facsimiles
                INNER JOIN systems ON systems.transcribe_id = facsimiles.id
                INNER JOIN system_relations ON system_relations.facsimile_id = facsimiles.id AND system_relations.system_id = systems.id
                INNER JOIN warps AS systemwarps ON system_relations.warp_id = systemwarps.id
                INNER JOIN staves ON staves.system_id = systems.id
                INNER JOIN warps AS staffwarps ON staves.warp_id = staffwarps.id
                WHERE
                    facsimiles.facsimile_set_id = %s AND
                    facsimiles.deleted_at IS NULL AND
                    systemwarps.deleted_at IS NULL AND
                    systems.deleted_at IS NULL AND 
                    staves.deleted_at IS NULL AND
                    staffwarps.deleted_at IS NULL
            ;''', (facsimile_set_id,))
            self._rows = pqdm(staves, parallel_methods._load_data, n_jobs=num_procs,
                desc="Loading data")
        self._untranscribed_staves = [row['staffid'] for row in sorted(self._rows, key=lambda r: (r['last_predicted'], r['staffid'])) if not row['transcribed']]
        if not hasattr(self, 'items'):
            self.items = {
                'stafflines': {},
                'syllables': {},
                'divisiones': {},
                'c_clefs': {},
                'f_clefs': {},
                'notes': {},
                'flat_accidentals': {},
                'sharp_accidentals': {}
            }
        for row in tqdm([row for row in self._rows if row['transcribed']], desc="Fetching items for transcribed rows"):
            self._fetch_items(row)
        for row in tqdm([row for row in self._rows if not row['transcribed']], desc="Initialising untranscribed rows"):
            self._no_items(row)
        for row in tqdm(self._rows, desc="Scaling"):
            self._do_scaling(row)
        # The dataframe to put the data in
        datagenerator_manager = multiprocessing.Manager()
        td_dataframe_name = name + training_data_queue_name
        td_dataframe_lock = datagenerator_manager.Lock()
        self._td_dataframe = self.add_dataframe(td_dataframe_name, dbfile=None,
                lock=td_dataframe_lock)
        #logging.warning("Making training data queue and consumers")
        queue = parallel_methods.make_queue(num_procs, training_data_queue_name)
        # A consumer process that takes from the queue and puts in the dataframe
        queue_consumer = [dataframe.DataFrameQueueConsumer(self._td_dataframe,
                queue, batch_size=batch_size, label=i) for i in range(num_procs)]
        # Will continue trying to consume until passed None
        #logging.warning("Starting training data queue consumers")
        for process in queue_consumer:
            process.name = "CANDR Training Data Queue Consumer"
            process.start()
        original_range = set(self._td_dataframe.all_ids())
        # Don't autocommit
        self._td_dataframe.start_batch()
        shared_items = datagenerator_manager.dict(self.items)
        parallel_methods._setup(self._max_h, shared_items)
        self.process(parallel_methods)
        #logging.warning("Ending training data queue")
        parallel_methods.end_queue(training_data_queue_name)
        for process in queue_consumer:
            process.join()
        #logging.warning("Committing training data to dataframe")
        # Do a big commit
        self._td_dataframe.end_batch()
        # Get all the IDs input into the dataframe
        self._range = self._td_dataframe.all_ids()
        self._new_range = list(set(self._range) - original_range)
        self.n_records = len(self._new_range)
        if hasattr(self, 'sample_ratio'):
            self.sample_ignore = np.where(np.array(self.sample_ratio)==0)
        self.on_epoch_end()
    def transform_untranscribed(self):
        return self.process(self._parallel_methods, False)
    def add_dataframe(self, name='default', dbfile=None, lock=None, **kwargs):
        if not hasattr(self, '_dataframe'):
            self._dataframe = {}
        if name not in self._dataframe or (dbfile is not None and
                self._dataframe[name]._dbfile != dbfile):
            self._dataframe[name] = dataframe.DataFrame(name=name,
                    dbfile=dbfile, lock=lock, **kwargs)
        elif lock:
            self._dataframe[name]._commit_lock = lock
        if self._dataframe[name]._commit is False:
            self._dataframe[name].end_batch()
        return self._dataframe[name]
    def process(self, parallel_methods, transcribed=True):
        #logging.warning("Beginning process()")
        staves_queue_name = 'staves'
        stave_dataframe_manager = multiprocessing.Manager()
        stave_dataframe_lock = stave_dataframe_manager.Lock()
        self._stave_dataframe = self.add_dataframe(str(self._facsimile_set_id) +
            staves_queue_name, lock=stave_dataframe_lock)
        num_procs = psutil.cpu_count()
        queue = parallel_methods.make_queue(num_procs, staves_queue_name)
        parallel_methods.staves_queue_name = staves_queue_name
        #logging.warning("Making queue consumers")
        queue_consumer = [dataframe.DataFrameQueueConsumer(
            self._stave_dataframe, queue, batch_size=self.batch_size, label=i) for i in range(num_procs)]
        for process in queue_consumer:
            process.name = "CANDR Dataframe Consumer"
            process.start()
        self._stave_dataframe.start_batch()
        rows_to_process = list([row for row in self._rows if row['transcribed'] == transcribed])
        initial_n = parallel_methods._initial_n() if transcribed else 0
        #logging.warning("Beginning progress bar")
        desc = None
        if transcribed:
            desc = "Processing staves and training data"
        else:
            desc = "Processing untranscribed staves"
        parallel_methods.begin_progress_bar(desc=desc)
        parallel_methods._progress_list_add_item((initial_n + 1) * len(rows_to_process))
        num_procs = psutil.cpu_count()
        init_pool = lambda: setattr(multiprocessing.current_process(), "name", "CANDR preprocessing worker")
        with multiprocessing.Pool(processes=num_procs, initializer=init_pool) as pool:
            if transcribed:
                rets = pool.map(parallel_methods._stave_process, rows_to_process)
            else:
                rets = pool.map(parallel_methods._untranscribed_stave_process, rows_to_process)
        del rows_to_process
        #logging.warning("Ending progress bar")
        parallel_methods.end_progress_bar()
        #logging.warning("Ending queue")
        parallel_methods.end_queue(staves_queue_name)
        #logging.warning("Ending queue consumers")
        for process in queue_consumer:
            process.join()
        self._stave_dataframe.end_batch()
        #logging.warning("Ending process()")
        return rets
    def _no_items(self, row):
        left = row["off"][0]
        top = row["off"][1]
        right = row["stop"][0]
        bottom = row["stop"][1]
        sid = row['staffid']
        its = ['stafflines', 'divisiones', 'syllables', 'c_clefs', 'f_clefs', 'flat_accidentals', 'sharp_accidentals', 'notes']
        for it in its:
            self.items[it][sid] = []
        self._max_h = max(self._max_h, self._calc_out_h([], left, top,
            right, bottom)[0])
        return
    def _fetch_items(self, row):
        left = row["off"][0]
        top = row["off"][1]
        right = row["stop"][0]
        bottom = row["stop"][1]
        sid = row["staffid"]
        ret = self._get_stafflines(sid)
        self.items['stafflines'][sid] = [[[s[0], s[1]], [s[2], s[3]]] for s in ret]
        self._max_h = max(self._max_h, self._calc_out_h(self.items['stafflines'][sid], left, top,
            right, bottom)[0])
        self.items['divisiones'][sid] = [((r[0], r[1]), (r[2], r[3])) for r in self._get_divisiones(sid)]
        self.items['syllables'][sid] = [{
            "top": c[0],
            "left": c[1],
            "width": c[2],
            "height": c[3],
            "angle": c[4],
            "centrepoint_x": c[5],
            "centrepoint_y": c[6]
        } for c in self._get_syllables(sid)]
        self.items['c_clefs'][sid] = [{
            "top": c[0],
            "left": c[1],
            "width": c[2],
            "height": c[3],
            "angle": c[4],
            "centrepoint_x": c[5],
            "centrepoint_y": c[6]
        } for c in self._get_c_clefs(sid)]
        self.items['f_clefs'][sid] = [{
            "top": f[0],
            "left": f[1],
            "width": f[2],
            "height": f[3],
            "angle": f[4],
            "centrepoint_x": f[5],
            "centrepoint_y": f[6]
        } for f in self._get_f_clefs(sid)]
        self.items['flat_accidentals'][sid] = [{
            "top": f[0],
            "left": f[1],
            "width": f[2],
            "height": f[3],
            "angle": f[4],
            "centrepoint_x": f[5],
            "centrepoint_y": f[6]
        } for f in self._get_flats(sid)]
        self.items['sharp_accidentals'][sid] = [{
            "top": f[0],
            "left": f[1],
            "width": f[2],
            "height": f[3],
            "angle": f[4],
            "centrepoint_x": f[5],
            "centrepoint_y": f[6]
        } for f in self._get_sharps(sid)]
        self.items['notes'][sid] = [[n[0], n[1]] for n in self._get_notes(sid)]
        return
    def _do_scaling(self, row):
        left = row["off"][0]
        top = row["off"][1]
        right = row["stop"][0]
        bottom = row["stop"][1]
        sid = row["staffid"]
        out_h, (left, top, right, bottom), extra, (addx, addy) = self._calc_out_h(self.items['stafflines'][sid], left, top, right, bottom)
        w = right - left
        assert self._max_h >= out_h
        #add_for_h = int(w * (self._max_h - out_h) / self.out_w)
        #add_for_top = int(add_for_h / 2)
        #add_for_bottom = add_for_h - add_for_top
        #top -= add_for_top
        #bottom += add_for_bottom
        h = bottom - top
        row['dims'] = (left, top, right, bottom)
        candr_log('dims', row['dims'], instance=self)
        # Scale of X and Y from original X/Y to new
        outxscale = float(self.out_w) / float(w)
        outyscale = float(self._max_h) / float(h)
        # Shift for staffline X and Y is the extra plus the min (which is negative)
        sx = extra - addx
        #sy = (extra - addy) + add_for_top
        sy = extra - addy
        def xscale(a):
            nonlocal sx, outxscale
            return (a + sx) * outxscale
        def yscale(a):
            nonlocal sy, outyscale
            return (a + sy) * outyscale
        def update_ret(a, **kwargs):
            a.update(**kwargs)
            return a
        self.scaling[sid] = ((outxscale, sx), (outyscale, sy))
        # Shift divisiones along then scale them to the correct size
        self.items['divisiones'][sid] = [[[int(xscale(s[0][0])),
            int(yscale(s[0][1]))], [int(xscale(s[1][0])),
                int(yscale(s[1][1]))]] for s in
            self.items['divisiones'][sid]]
        # Shift stafflines along then scale them to the correct size
        self.items['stafflines'][sid] = [[[int(xscale(s[0][0])),
            int(yscale(s[0][1]))], [int(xscale(s[1][0])),
                int(yscale(s[1][1]))]] for s in
            self.items['stafflines'][sid]]
        # Shift syllables along then scale them to the correct size
        self.items['syllables'][sid] = [update_ret(s, top=yscale(s['top']),
            left=xscale(s['left']), width=(s['width'] * outxscale),
            height=(s['height'] * outyscale),
            centrepoint_x=xscale(s['centrepoint_x']),
            centrepoint_y=yscale(s['centrepoint_y'])) for s in self.items['syllables'][sid]]
        # Shift c_clefs along then scale them to the correct size
        self.items['c_clefs'][sid] = [update_ret(s, top=yscale(s['top']),
            left=xscale(s['left']), width=(s['width'] * outxscale),
            height=(s['height'] * outyscale),
            centrepoint_x=xscale(s['centrepoint_x']),
            centrepoint_y=yscale(s['centrepoint_y'])) for s in self.items['c_clefs'][sid]]
        # Shift f_clefs along then scale them to the correct size
        self.items['f_clefs'][sid] = [update_ret(s, top=yscale(s['top']),
            left=xscale(s['left']), width=(s['width'] * outxscale),
            height=(s['height'] * outyscale),
            centrepoint_x=xscale(s['centrepoint_x']),
            centrepoint_y=yscale(s['centrepoint_y'])) for s in self.items['f_clefs'][sid]]
        # Shift sharp_accidentals along then scale them to the correct size
        self.items['sharp_accidentals'][sid] = [update_ret(s,
            top=yscale(s['top']), left=xscale(s['left']),
            width=(s['width'] * outxscale),
            height=(s['height'] * outyscale),
            centrepoint_x=xscale(s['centrepoint_x']),
            centrepoint_y=yscale(s['centrepoint_y'])) for s in self.items['sharp_accidentals'][sid]]
        self.items['flat_accidentals'][sid] = [update_ret(s,
            top=yscale(s['top']), left=xscale(s['left']),
            width=(s['width'] * outxscale),
            height=(s['height'] * outyscale),
            centrepoint_x=xscale(s['centrepoint_x']),
            centrepoint_y=yscale(s['centrepoint_y'])) for s in self.items['flat_accidentals'][sid]]
        self.items['notes'][sid] = [[int(xscale(s[0])), int(yscale(s[1]))]
            for s in self.items['notes'][sid]]
        return
    def _get_stafflines(self, sid):
        return self._do_sql(r'''SELECT stafflines.ax, stafflines.ay, stafflines.bx, stafflines.by FROM items
            INNER JOIN stafflines ON items.itemtype_id = stafflines.id
            WHERE 
                items.stave_id = %s AND
                items.itemtype_type = 'App\Models\Staffline' AND
                items.deleted_at IS NULL AND
                stafflines.deleted_at IS NULL
        ;''', (sid,))
    def _get_divisiones(self, sid):
        return self._do_sql(r'''SELECT divisiones.ax, divisiones.ay, divisiones.bx, divisiones.by FROM items
            INNER JOIN divisiones on items.itemtype_id = divisiones.id
            WHERE
                items.stave_id = %s AND
                items.itemtype_type = 'App\Models\Divisione' AND
                items.deleted_at IS NULL AND
                divisiones.deleted_at IS NULL
        ;''', (sid,))
    def _get_syllables(self, sid):
        return self._do_sql(r'''SELECT syllables.top, syllables.left, syllables.width, syllables.height, syllables.angle, syllables.centrepoint_x, syllables.centrepoint_y FROM items
            INNER JOIN syllables on items.itemtype_id = syllables.id
            WHERE
                items.stave_id = %s AND
                items.itemtype_type = 'App\Models\Syllable' AND
                items.deleted_at IS NULL AND
                syllables.deleted_at IS NULL
        ;''', (sid,))
    def _get_c_clefs(self, sid):
        return self._do_sql(r'''SELECT clefs.top, clefs.left, clefs.width, clefs.height, clefs.angle, clefs.centrepoint_x, clefs.centrepoint_y FROM items
            INNER JOIN clefs on items.itemtype_id = clefs.id
            WHERE
                items.stave_id = %s AND
                items.itemtype_type = 'App\Models\Clef' AND
                clefs.type = 'C' AND
                items.deleted_at IS NULL AND
                clefs.deleted_at IS NULL
        ;''', (sid,))
    def _get_f_clefs(self, sid):
        return self._do_sql(r'''SELECT clefs.top, clefs.left, clefs.width, clefs.height, clefs.angle, clefs.centrepoint_x, clefs.centrepoint_y FROM items
            INNER JOIN clefs on items.itemtype_id = clefs.id
            WHERE
                items.stave_id = %s AND
                items.itemtype_type = 'App\Models\Clef' AND
                clefs.type = 'F' AND
                items.deleted_at IS NULL AND
                clefs.deleted_at IS NULL
        ;''', (sid,))
    def _get_flats(self, sid):
        return self._do_sql(r'''SELECT accidentals.top, accidentals.left, accidentals.width, accidentals.height, accidentals.angle, accidentals.centrepoint_x, accidentals.centrepoint_y FROM items
            INNER JOIN accidentals on items.itemtype_id = accidentals.id
            WHERE
                items.stave_id = %s AND
                items.itemtype_type = 'App\Models\Accidental' AND
                accidentals.type = 'Flat' AND
                items.deleted_at IS NULL AND
                accidentals.deleted_at IS NULL
        ;''', (sid,))
    def _get_sharps(self, sid):
        return self._do_sql(r'''SELECT accidentals.top, accidentals.left, accidentals.width, accidentals.height, accidentals.angle, accidentals.centrepoint_x, accidentals.centrepoint_y FROM items
            INNER JOIN accidentals on items.itemtype_id = accidentals.id
            WHERE
                items.stave_id = %s AND
                items.itemtype_type = 'App\Models\Accidental' AND
                accidentals.type = 'Sharp' AND
                items.deleted_at IS NULL AND
                accidentals.deleted_at IS NULL
        ;''', (sid,))
    def _get_notes(self, sid):
        return self._do_sql(r'''SELECT notes.x, notes.y FROM items
            INNER JOIN notes ON items.itemtype_id = notes.id
            WHERE
                items.stave_id = %s AND
                items.itemtype_type = 'App\Models\Note' AND
                items.deleted_at IS NULL AND
                notes.deleted_at IS NULL
        ;''', (sid,))

    def _calc_out_h(self, stafflines, left, top, right, bottom):
        # Create array of stafflines and find min/max values
        if len(stafflines) == 0:
            minx = miny = 0
            maxx = right - left
            maxy = bottom - top
        else:
            minx = miny = float('inf')
            maxx = maxy = float('-inf')
        for staffline in stafflines:
            ax = staffline[0][0]
            ay = staffline[0][1]
            bx = staffline[1][0]
            by = staffline[1][1]
            minx = min(minx, ax, bx)
            miny = min(miny, ay, by)
            maxx = max(maxx, ax, bx)
            maxy = max(maxy, ay, by)
        addx = addy = 0
        # Calculate width and height of stave box
        original_h = h = bottom - top
        original_w = w = right - left
        # If stafflines overflow the stave box, make the stave box bigger
        if minx < 0:
            addx = minx
        if miny < 0:
            addy = miny
        left += addx
        top += addy
        if maxx > w:
            right += (maxx - w)
        if maxy > h:
            bottom += (maxy - h)
        # Recalculate stave box
        w = right - left
        h = bottom - top
        # Ratio of stave box width to height
        ratio = float(w) / float(h)
        wmul = self.mul / ratio
        hmul = self.mul * ratio
        # Add on extra width and height as border
        extraw = int(np.ceil(w * wmul))
        extrah = int(np.ceil(h * hmul))
        extra = max(extraw, extrah)
        left -= extra
        right += extra
        top -= extra
        bottom += extra
        # Calculate height from set width
        out_h = int(self.out_w * h / float(w))
        dims = (left, top, right, bottom)
        addxy = (addx, addy)
        candr_log("calc_out_h", out_h, dims, extra, addxy, instance=self)
        return (out_h, dims, extra, addxy)
    def __del__(self):
        if hasattr(self, '_dataframe'):
            del self._dataframe
        if hasattr(self, '_range'):
            del self._range
    def on_epoch_end(self):
        """Executed after every epoch"""
        if self.shuffle == True and hasattr(self, '_range'):
            random.shuffle(self._range)
    def __len__(self):
        """Keras method to find the number of batches"""
        return len(self._new_range)
    def __getitem__(self, index):
        """Dummy method, to be implemented by children"""
        return [], []
    def _do_sql(self, query, inp):
        """Wrapper for SQL.

            Parameters:
                query (string): SQL query
                inp (tuple):    SQL replacements

            Returns:
                ret: SQL execution result
        """
        with self._sql_lock:
            self._cursor.execute(query, inp)
            return self._cursor.fetchall()
class GenericDataGeneratorExecutor:
    _pickle_me = ['_setup', '_initial_n', '_progress_list',
        '_progress_list_add_item', '_progress_list_complete_item',
        '_progress_list_lock', '_stave_process', '_stave_transform',
        'otsu', '_bucket', 'out_w', '_max_h', 'staves_queue_name', '_pid',
        'training_data_queue_name', 'supersample', 'window_size', 'step',
        '_items', '_looper', '_queue',  '_untranscribed_stave_process']
    def __init__(self, out_w, bucket, supersample=5, window_size=40,
            batch_size=32, step=4, training_data_queue_name='training_data'):
        self._multiprocessing_manager = multiprocessing.Manager()
        self._bucket = bucket
        self.training_data_queue_name = training_data_queue_name
        self._pid = None
        self.staves_queue_name = 'staves'
        self.out_w = out_w
        self.supersample = supersample
        self.window_size = window_size
        self.batch_size = batch_size
        self.step = step
        self._max_h = 0
        self._items = {}
    def _setup(self, max_h, items):
        self._max_h = max_h
        self._items = items
    def __getstate__(self):
        return {k: v for k, v in self.__dict__.items() if k in self._pickle_me}
    def __setstate__(self, state):
        self.__dict__.update(state)
        self._pid = str(uuid.uuid4())
        return
    def begin_progress_bar(self, desc=None):
        kwargs = {"desc": desc}
        self._progress_process = multiprocessing.Process(target=self._progress_bar_updater,kwargs=kwargs)
        self._progress_list_lock = self._multiprocessing_manager.Lock()
        self._progress_list = self._multiprocessing_manager.list([0, 0, False])
        self._progress_process.name = "CANDR progress bar updater"
        self._progress_process.start()
    def _bucket_get_image(self, im):
        return self._bucket.get_image(im)
    def _progress_bar_updater(self, desc=None):
        with tqdm(total=0, desc=desc) as pbar:
            while True:
                if any(p < 0 for p in self._progress_list):
                    break
                if len(self._progress_list) != 3:
                    raise Exception("Progress list must be of length 3, but we have length " + str(len(self._progress_list)))
                while self._progress_list[2] == True:
                    with self._progress_list_lock:
                        self._progress_list[2] = False
                        pbar.total = self._progress_list[1]
                        pbar.n = self._progress_list[0]
                        pbar.last_print_n = self._progress_list[0]
                        pbar.refresh()
                #logging.warning("No progress bar updates. Sleeping.")
                time.sleep(1)
        return
    def _progress_list_add_item(self, n=1):
        with self._progress_list_lock:
            self._progress_list[1] += n
            self._progress_list[2] = True
    def _progress_list_complete_item(self, n=1):
        with self._progress_list_lock:
            self._progress_list[0] += n
            self._progress_list[2] = True
    def end_progress_bar(self):
        with self._progress_list_lock:
            self._progress_list[0] = -1
            self._progress_list[1] = -1
            self._progress_list[2] = True
        self._progress_process.join()
    def _initial_n(self):
        return 0
    def make_queue(self, length=1, name='default'):
        if not hasattr(self, '_queue'):
            self._queue = {}
        self._queue[name] = self._multiprocessing_manager.Queue(length)
        return self._queue[name]
    def end_queue(self, name='default'):
        if not hasattr(self, '_queue'):
            return True
        if name not in self._queue:
            return True
        self._queue[name].put(None)
        return True
    def _looper(self, staffid):
        candr_log('Entered abstract _looper, but it doesn\'t do anything',
                instance=self)
        return None
    def _untranscribed_stave_process(self, row):
        return self._stave_process(row, False)
    def _stave_process(self, row, with_looper=True):
        #logging.warning(str(self._pid) + ': Entered _stave_process')
        self._progress_list_add_item(2)
        system_transformed = self._bucket_get_transform(row['systemcoeffs'], row['image'])
        self._progress_list_complete_item()
        stave_transformed = self._stave_transform(system_transformed,
            (self.out_w, self._max_h), row['staffcoeffs'], row['dims'])
        self._queue[self.staves_queue_name].put([(row['staffid'], stave_transformed)])
        self._progress_list_complete_item()
        ret = None
        if with_looper:
            ret = self._looper(row['staffid'])
        self._progress_list_complete_item()
        #logging.warning(str(self._pid) + ': Exiting _stave_process')
        return ret
    def _bucket_get_transform(self, systemcoeffs, image):
        im = self._bucket_get_image(image)
        return self._system_transform(systemcoeffs, im)
    def _system_transform(self, systemcoeffs, imaged):
        #logging.warning(str(self._pid) + ': Entered _system_transform')
        width, height = imaged.size
        ret = imaged.copy().transform((width, height), Image.PERSPECTIVE, systemcoeffs, Image.BICUBIC)
        #logging.warning(str(self._pid) + ': Exiting _system_transform')
        return ret
    def _stave_transform(self, im, resize, staffcoeffs, dims):
        #logging.warning(str(self._pid) + ': Entered _stave_transform')
        im = im.copy().transform(im.size, Image.PERSPECTIVE, staffcoeffs,
            Image.BICUBIC).crop(dims).resize(resize, Image.ANTIALIAS)
        staff_dat = np.array(im, dtype='float') / 255.0
        normalized = (staff_dat - np.min(staff_dat))/np.ptp(staff_dat)
        #logging.warning(str(self._pid) + ': Exiting _stave_transform')
        return normalized
    def _load_data(self, row):
        def tltrblbr(points):
            """Sort a quad into corners.

                Parameters:
                    points (list of list(2) of int): Points to be sorted

                Returns:
                    points (list of list(2) of int): Sorted points in order tl, tr, bl, br
            """
            points.sort(key=lambda point: point[1])
            tl = points[0] if points[0][0] < points[1][0] else points[1]
            tr = points[0] if points[0][0] >= points[1][0] else points[1]
            bl = points[2] if points[2][0] < points[3][0] else points[3]
            br = points[2] if points[2][0] >= points[3][0] else points[3]
            ret = [tl, tr, bl, br]
            return ret
        def minrect(points):
            """Calculate smallest rectangle that encompasses a set of points.

                Parameters:
                    points (list of list(2) of int): Points for rectangle to be sorted

                Returns:
                    points (list of list(2) of int): Minimum rectangle
            """
            points.sort(key=lambda point: point[1])
            minx = miny = float('inf')
            maxx = maxy = float('-inf')
            for point in points:
                if point[0] < minx:
                    minx = point[0]
                if point[1] < miny:
                    miny = point[1]
                if point[0] > maxx:
                    maxx = point[0]
                if point[1] > maxy:
                    maxy = point[1]
            return [
                [minx, miny], [maxx, miny],
                [minx, maxy], [maxx, maxy]
            ]
        def find_coeffs(pb, pa):
            """Calculate the coefficients for perspective transform.

                Parameters:
                    pb (list of list(2) of int): Points to transform from
                    pa (list of list(2) of int): Points to transform to

                Returns:
                    coeffs (np.array(8)): Coefficients
            """
            matrix = []
            for p1, p2 in zip(pa, pb):
                matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0]*p1[0], -p2[0]*p1[1]])
                matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1]*p1[0], -p2[1]*p1[1]])
            A = np.matrix(matrix, dtype=np.float)
            B = np.array(pb).reshape(8)
            try:
                C = np.linalg.inv(A.T * A)
            except:
                return np.array([1, 0, 0, 0, 1, 0, 0, 0, 0])
            D = C * A.T
            X = np.array(np.dot(D, B)).reshape(8)
            return X
        # System ID
        systemid = row[0]
        # Staff ID
        staffid = row[1]
        # Transcribed?
        transcribed = bool(row[2])
        # last_predicted
        last_predicted = row[3]
        # Image fname
        imaged = row[4]

        # Load system warp from JSON
        systemnodes = tltrblbr(json.loads(row[5]))
        # Find minrect of this warp
        systemnodeminrect = tltrblbr(minrect(systemnodes.copy()))
        # Load staff warp from JSON
        staffnodes = tltrblbr(json.loads(row[6]))
        # Offset the staff warp from the system minrect
        for node in staffnodes:
            node[0] += systemnodeminrect[0][0]
            node[1] += systemnodeminrect[0][1]
        # Find minrect of this warp
        staffnodeminrect = tltrblbr(minrect(staffnodes.copy()))
        # find coeffs for system and staff transformation
        systemcoeffs = find_coeffs(systemnodes, systemnodeminrect)
        staffcoeffs = find_coeffs(staffnodes, staffnodeminrect)
        # Create a record with this data
        return {"last_predicted": last_predicted, "transcribed": transcribed,
            "systemid": systemid, "staffid": staffid,
            "systemcoeffs": systemcoeffs, "staffcoeffs": staffcoeffs,
            "image": imaged, "off": staffnodeminrect[0],
            "stop": staffnodeminrect[3]}
