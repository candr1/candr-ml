import numpy as np
from . import generic
import random
from PIL import Image, ImageDraw

class StafflineDataGeneratorExecutor(generic.GenericDataGeneratorExecutor):
    def __init__(self, bucket, window_size=40, supersample=5, out_w=1000,
            staffline_w=5, batch_size=32, step=4,
            training_data_queue_name='training_data'):
        self.staffline_w = staffline_w
        super(StafflineDataGeneratorExecutor, self).__init__(out_w, bucket, 
                supersample, window_size, batch_size, step,
                training_data_queue_name)
        self._pickle_me.extend(['staffline_w'])
    def _initial_n(self):
        # Number of calls to _progress_list_complete_item in _looper
        return 1
    def _looper(self, staffid):
        staffline_w = self.staffline_w
        supersample = self.supersample
        max_h = self._max_h
        out_w = self.out_w
        stafflines = self._items['stafflines']
        window_size = self.window_size
        step = self.step
        queue_name = self.training_data_queue_name
        sample_ratio = [0, 0]
        line_w = staffline_w * supersample
        shape = (max_h, out_w)
        if len(stafflines[staffid]) > 0:
            mask = Image.new('L', tuple([shape[1] * supersample, shape[0] * supersample]))
            draw = ImageDraw.Draw(mask)
            for staffline in stafflines[staffid]:
                line = [tuple([x * supersample for x in p]) for p in staffline]
                draw.line(line, fill=255, width=line_w)
            npim = np.array(mask.resize((shape[1], shape[0]), Image.ANTIALIAS)
                ).reshape(shape).astype('float')
            yy = (npim - np.min(npim))/np.ptp(npim)
            del mask
            del draw
            del npim
        else:
            yy = np.zeros(shape, dtype='float')
        #image.export_image(yy, "pixel-mask-" + str(staffid) + ".png")
        # Do a moving window over each X and Y
        half_window = window_size // 2
        pre_proc = []
        a_jit = random.randint(0, step)
        b_jit = random.randint(0, step)
        for x_start in range(a_jit, out_w - window_size, step):
            for y_start in range(b_jit, max_h - window_size, step):
                x_end = x_start + window_size
                y_end = y_start + window_size
                x_mid = x_start + half_window
                y_mid = y_start + half_window
                # Are any stafflines visible here?
                val = yy[y_mid,x_mid]
                one_hot = [val, 1 - val]
                sample_ratio[0] += val
                sample_ratio[1] += 1 - val
                # Flip data in both axes for more data
                #flips = [(), (0,), (1,), (0,1)]
                flips = [()]
                for flip in flips:
                    pre_proc.append({
                        "sid": staffid,
                        "window": (x_start, y_start),
                        "dist": one_hot,
                        "flip": flip
                    })
        self._queue[queue_name].put(pre_proc)
        del yy
        self._progress_list_complete_item()
        return sample_ratio
class StafflineGenerator(generic.GenericDataGenerator):
    """Data generator for finding pixels"""

    def __init__(self, pg_conn, bucket, facsimile_set_id, out_w=1000, mul=0.02,
            window_size=40, batch_size=32, step=4, staffline_w=5, supersample=5,
            shuffle=True, training_data_queue_name="training_data"):
        """Initialise generator.

            Parameters:
                pg_conn (psycopg2 conn): Database connection
                bucket (CloudImage):     Google Cloud storage bucket to access images
                facsimile_set_id (int):  Database ID of the facsimile set to train for
                out_w (int):             Basic stave width
                mul (float):             Multiplier for border
                window_size (int):       Size of the moving window for training
                batch_size (int):        Number of samples to yield
                step (int):              How quickly the moving window moves
                shuffle (bool):          Randomise samples after each epoch
        """
        self.window_size = window_size
        self.step = step
        self.staffline_w = staffline_w
        self.supersample = supersample
        self.idx_names = ["staffline", "nothing"]
        self.idxs = {v: i for i, v in enumerate(self.idx_names)}
        self.sample_ratio = [0, 0]

        parallel_methods = StafflineDataGeneratorExecutor(bucket, window_size,
                supersample, out_w, staffline_w, batch_size, step,
                training_data_queue_name)

        # Send this to the generic data generator
        super(StafflineGenerator, self).__init__(pg_conn, parallel_methods,
            facsimile_set_id, out_w=out_w, mul=mul, batch_size=batch_size,
            shuffle=shuffle, name=self.__class__.__name__,
            training_data_queue_name=training_data_queue_name)
    def process(self, parallel_methods, transcribed=True):
        ret = super(StafflineGenerator, self).process(parallel_methods,
                transcribed)
        if transcribed and len(ret) > 0:
            summed = [sum(i) for i in zip(*ret)]
            self.sample_ratio[0] += summed[0]
            self.sample_ratio[1] += summed[1]
        return
    def __getitem__(self, index):
        # Get one batch's worth of data
        idxs = self._new_range[index]
        inp = self._td_dataframe.get(idxs, use_lock=False)
        inplen = len(inp)
        X = np.empty((inplen, self.window_size, self.window_size, 3), dtype='float')
        Y = np.empty((inplen, 2), dtype='float')
        staff_data = {}
        for sid in set([record['sid'] for record in inp]):
            staff_data[sid] = self._stave_dataframe.get(sid, use_lock=False)
        for idx, record in enumerate(inp):
            window = record["window"]
            sid = record['sid']
            x_start = window[0]
            y_start = window[1]
            x_end = x_start + self.window_size
            y_end = y_start + self.window_size
            # Do the flip described
            seg = np.flip(staff_data[sid][y_start:y_end,x_start:x_end], record["flip"])
            X[idx] = seg.reshape(self.window_size, self.window_size, 3)
            Y[idx] = record["dist"]
        del inp
        del staff_data
        return X, Y
