from tensorflow import keras
from . import abstract
from ..generators import item
from ..misc import image
from ..misc.candr_log import candr_log
import numpy as np
from scipy import ndimage
import cv2
import json
import psutil

class ItemModel(abstract.AbstractModel):
    def __init__(self, pg_conn, bucket, facsimile_set_id, out_w=1000, mul=0.02,
            window_size=20, batch_size=32, step=4, shuffle=True,
            clef_min_size=400, clef_max_size=1000, accidental_min_size=400,
            accidental_max_size=1000, syllable_min_size=400,
            syllable_max_size=400, note_radius=6, divisione_w=3):
        gen = item.ItemDataGenerator(pg_conn, bucket, facsimile_set_id,
            out_w=out_w, mul=mul, window_size=window_size,
            batch_size=batch_size, step=step, shuffle=shuffle,
            note_radius=note_radius, divisione_w=divisione_w)
        self.sizes = {
            "c_clefs": (clef_min_size, clef_max_size),
            "f_clefs": (clef_min_size, clef_max_size),
            "flats": (accidental_min_size, accidental_max_size),
            "sharps": (accidental_min_size, accidental_max_size),
            "syllables": (syllable_min_size, syllable_max_size)
        }
        super(ItemModel, self).__init__(gen, step, window_size, batch_size)
        if self._generator.n_records > 0:
            input_shape = (self._generator.window_size, self._generator.window_size, 3)
            self.model = keras.models.Sequential()
            self.model.add(keras.layers.Conv2D(32, 3, activation='relu', input_shape=input_shape))
            self.model.add(keras.layers.MaxPooling2D((2, 2)))
            self.model.add(keras.layers.Conv2D(32, 3, activation='relu', input_shape=input_shape))
            self.model.add(keras.layers.MaxPooling2D((2, 2)))
            self.model.add(keras.layers.Flatten())
            self.model.add(keras.layers.Dense(256, activation='relu'))
            self.model.add(keras.layers.Dense(len(self._generator.idxs), activation='sigmoid'))
            optim = keras.optimizers.Adam(learning_rate=0.0001)
            self.model.compile(optimizer=optim, loss='categorical_crossentropy', metrics=['accuracy'])
            self.model.summary()
            self.trainable = True
    def train(self, epochs=1, epoch_callback=lambda: None):
        if self.trainable:
            ratio = self._generator.sample_ratio
            total = sum(ratio)
            weights = [(0 if i == 0 else ((1.0 / i)*total)) for i in ratio]
            print("Weights:", json.dumps(weights))
            super().train(epochs, epoch_callback, weights=weights)
    def predict(self, staff_ids, step=None, despeckle=None, batch_info=tuple()):
        candr_log("predict", instance=self)
        cpus = psutil.cpu_count()
        pd_items = self._make_predictor_items(ndf=cpus, npd=1)
        pred_kwargs = {}
        super_kwargs = {
            "batch_info": batch_info
        }
        if step:
            pred_kwargs['pred_step'] = step
            super_kwargs['step'] = step
        if despeckle:
            pred_kwargs['pred_despeckle'] = despeckle
            super_kwargs['despeckle'] = despeckle
        predictors = [ItemPredictor(self._generator.note_radius, self.step,
                self._generator.window_size, self.batch_size,
                self._generator.idxs, self._generator.idx_names,
                self._generator.sample_ignore, self.sizes,
                self._generator.scaling, pd_items,
                **pred_kwargs) for i in range(cpus)]
        return super(ItemModel, self).predict(staff_ids, predictors, pd_items,
                **super_kwargs)
class ItemPredictor(abstract.AbstractPredictor):
    def __init__(self, note_radius, step, window_size, batch_size, idxs,
            idx_names, sample_ignore, sizes, scaling, pd_items, pred_step=1,
            pred_despeckle=None):
        self.note_radius = note_radius
        self.idxs = idxs
        self.idx_names = idx_names
        self.sample_ignore = sample_ignore
        self.sizes = sizes
        if not pred_despeckle:
            pred_despeckle = note_radius - 1
        super(ItemPredictor, self).__init__(step, window_size, batch_size,
                scaling, pd_items, pred_step, pred_despeckle)
    def predict(self, staff_id):
        candr_log("predict " + str(staff_id), instance=self)
        staff_dat = self.get_staff_data(staff_id)
        this_step = self.pred_step if self.pred_step is not None else self.step
        nidxs = len(self.idxs)
        predict_out = self.do_prediction(staff_dat)
        heatmap = np.zeros((staff_dat.shape[0], staff_dat.shape[1], nidxs), dtype='float')
        del staff_dat
        half_window = self.window_size // 2
        quart_window = half_window // 2
        radial_x = np.linspace(-1, 1, half_window)[:, None]
        radial_y = np.linspace(-1, 1, half_window)[None, :]
        radial = np.sqrt(radial_x ** 2 + radial_y ** 2)
        radial = np.max(radial) - radial
        radial = (radial - np.min(radial))/np.ptp(radial)
        heatmap[:,:,self.idxs['nothing']] = np.nextafter(0, 1)
        #Nullify any dangling predictions from ignored samples
        predict_out[:,:,self.sample_ignore] = 0
        for idx in np.ndindex(predict_out.shape[0], predict_out.shape[1]):
            #y_start = idx[0] * this_step + quart_window
            #y_end = y_start + half_window
            #x_start = idx[1] * this_step + quart_window
            #x_end = x_start + half_window
            y_start = idx[0] * this_step + half_window
            y_end = (idx[0] + 1) * this_step + half_window
            x_start = idx[1] * this_step + half_window
            x_end = (idx[1] + 1) * this_step + half_window
            #channel = np.argmax(predict_out[idx[0],idx[1]])
            #heatmap[y_start:y_end,x_start:x_end,channel] += radial
            heatmap[y_start:y_end,x_start:x_end] += predict_out[idx[0], idx[1]]
        del predict_out
        decided_heatmap = np.zeros(heatmap.shape, dtype='uint8')
        for y in range(0, heatmap.shape[0]):
            for x in range(0, heatmap.shape[1]):
                brightest = np.argmax(heatmap[y,x])
                decided_heatmap[y,x,brightest] = 1
        del heatmap
        by_feature = np.transpose(decided_heatmap, (2, 0, 1))
        masses = np.ones((decided_heatmap.shape[0],
            decided_heatmap.shape[1]), dtype='uint8')
        del decided_heatmap
        ret = {}
        for idx, feature in enumerate(by_feature):
            feature_name = self.idx_names[idx]
            ptp = np.ptp(feature)
            if ptp == 0:
                normalized = 255 * feature
            else:
                normalized = 255 * (feature - np.min(feature))/np.ptp(feature)
            #image.export_image(normalized, "item-" + feature_name + "-heatmap-" + str(staff_id) + ".png")
            # Threshold heatmap
            otsu = image.otsu(ndimage.median_filter(normalized,
                size=self.pred_despeckle)).astype('uint8')
            del normalized
            #image.export_image(otsu, "item-" + feature_name + "-mask-" + str(staff_id) + ".png")
            # Label the features extracted
            labels, nlabels = ndimage.label(otsu)
            sizes = (0, np.inf)
            label_use = range(1, nlabels + 1) if (nlabels > 1) else []
            if feature_name in self.sizes.keys():
                sizes = self.sizes[feature_name]
            if feature_name == 'notes':
                t_iterations = (self.pred_despeckle // 2) if self.pred_despeckle != 0 else 0
                if nlabels > 1:
                    structure = np.ones((3, 3), dtype=np.uint8)
                    eroded = (otsu > 1).astype(np.uint8)
                    t_labels = labels
                    t_nlabels = nlabels
                    t_erosion = 0
                    best_erosion = 0
                    best_nlabels = nlabels
                    best_labels = labels
                    while np.any(eroded):
                        t_erosion += 1
                        eroded = ndimage.binary_erosion(eroded, structure)
                        re_dilated = ndimage.binary_dilation(eroded, structure,
                            iterations=t_iterations)
                        t_labels, t_nlabels = ndimage.label(re_dilated)
                        if t_nlabels > best_nlabels:
                            best_nlabels = t_nlabels
                            best_labels = t_labels
                            best_erosion = t_erosion
                        del re_dilated
                        del t_labels
                    del eroded
                    candr_log("Best erosion was " + str(best_erosion) + " (" +
                        str(best_nlabels) + " vs " + str(nlabels) + " labels)",
                        instance=self)
                    #image.export_image((best_labels != 0).astype(np.uint8),
                    #    "item-" + feature_name + "-mask-eroded-" + str(staff_id)
                    #    + ".png")
                    ret['notes'] = [self._unscale_and_shift(staff_id, a[0], a[1]) for a in (
                        np.array(
                            ndimage.center_of_mass(
                                masses,
                                best_labels,
                                index=list(range(1, best_nlabels + 1)))
                        )[:,[1,0]]).tolist()]
                    del best_labels
                else:
                    ret['notes'] = []
            elif feature_name == 'divisiones':
                min_line_length = self.window_size * 2
                lines = []
                for i in label_use:
                    # Get an array of coordinates of that feature
                    coords = np.array(np.where(labels==i)).T
                    ycoords = coords[:,0]
                    # Find the coords with the minimum and maximum x-coordinate
                    minc = coords[np.argmin(ycoords)]
                    maxc = coords[np.argmax(ycoords)]
                    # Calculate the length of that line
                    line_len = np.linalg.norm(maxc - minc)
                    if line_len < min_line_length:
                        candr_log("Found segment with length " +
                            str(int(line_len)) + " but minimum line length was "
                            + str(int(min_line_length)), instance=self)
                        continue
                    # Swap x and y, convert to list
                    a = minc[[1,0]].tolist()
                    a = self._unscale_and_shift(staff_id, a[0], a[1])
                    b = maxc[[1,0]].tolist()
                    b = self._unscale_and_shift(staff_id, b[0], b[1])
                    lines.append((a, b))
                    del coords
                ret['divisiones'] = lines
            elif feature_name == 'nothing':
                # do nothing obviously!
                continue
            else:
                # Must be box
                box_feature = []
                sqrtminsize = np.inf
                if sizes:
                    sqrtminsize = np.sqrt(sizes[0])
                # For every feature
                for i in label_use:
                    # Find the minimum area
                    mask = labels.copy().astype('uint8')
                    mask[mask!=i] = 0
                    _, cnt, hierarchy = cv2.findContours(mask,
                        cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                    del mask
                    cnt = cnt[0]
                    area = cv2.contourArea(cnt)
                    if not sizes[0] <= area <= sizes[1]:
                        continue
                    box = cv2.minAreaRect(cnt)
                    centre_x, centre_y = self._unscale_and_shift(staff_id, box[0][0], box[0][1])
                    width, height = self._unscale(staff_id, box[1][0], box[1][1])
                    angle = box[2]
                    if width < sqrtminsize or height < sqrtminsize:
                        continue
                    top = centre_y - height / 2
                    left = centre_x - width / 2
                    box_feature.append({
                        "top": top,
                        "left": left,
                        "width": width,
                        "height": height,
                        "angle": angle,
                        "centrepoint_x": centre_x,
                        "centrepoint_y": centre_y
                    })
                ret[feature_name] = box_feature
            del otsu
            del labels
        del by_feature
        del masses
        return ret
