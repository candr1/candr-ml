import numpy as np
import gc
import queue
from tqdm import tqdm
import multiprocessing
from pympler import tracker
from ..misc import callbacks, duplex_queue, dataframe
from ..misc.candr_log import candr_log
class AbstractPredictorDuplexQueueConsumer(duplex_queue.AbstractDuplexQueueConsumer):
    def __init__(self, queue, model, window, nidxs, step, batch_size,
            label=None):
        # This is a special consumer for doing prediction on the main thread
        super(AbstractPredictorDuplexQueueConsumer, self).__init__(queue, label)
        self._model = model
        self._window = window
        self._step = step
        self._batch_size = batch_size
        self._nidxs = nidxs
        self._tracker = tracker.SummaryTracker()
    def consume(self, staff_dat):
        candr_log("consuming staff_dat", instance=self)
        # Calculate our data into batches
        dat, gen, rem = self.dat_gen_rem(staff_dat, self._window, self._step,
                self._batch_size)
        # Predict
        prediction =  self.predict(gen, dat['batches'], rem).reshape(dat['h_samples'],
                dat['w_samples'], self._nidxs)
        del staff_dat
        del dat
        del gen
        del rem
        gc.collect()
        #print(self.__class__.__name__ + " " +
        #        str(multiprocessing.current_process().pid) + " summary:\n" +
        #        "\n".join(self._tracker.format_diff()))
        return prediction
    def predict(self, generator, nbatches, remainder):
        candr_log("doing predict", instance=self)
        # Do the single-threaded prediction
        batches = self._model.predict(generator, steps=nbatches)
        # Predict any remainder
        if remainder.shape[0] > 0:
            rem_pred = self._model.predict(remainder)
            concat = np.concatenate([batches, rem_pred])
            del batches
            del rem_pred
            return concat
        return batches
    def _make_dat(self, arrinput, window, step=1, batch_size=32):
        width = arrinput.shape[1] - window
        height = arrinput.shape[0] - window
        h_samples = height // step
        w_samples = width // step
        samples = h_samples * w_samples
        batch_step = batch_size * step
        batches = samples // batch_size
        remainder = samples % batch_size
        remainder_idx_start = batches * batch_size
        remainder_x_start = (remainder_idx_start // h_samples) * step
        remainder_y_start = (remainder_idx_start % h_samples) * step
        assert remainder_x_start <= width
        assert remainder_y_start <= height
        return {
            "width": width,
            "height": height,
            "h_samples": h_samples,
            "w_samples": w_samples,
            "samples": samples,
            "batch_step": batch_step,
            "batches": batches,
            "remainder": remainder,
            "remainder_start": (remainder_y_start, remainder_x_start)
        }
    def _predict_generator(self, arrinput, samples, w_samples, window, step=1, batch_size=32):
        for batch_num, batch_sample_start in enumerate(range(0, samples, batch_size)):
            batch_sample_end = batch_sample_start + batch_size
            yieldme = np.zeros((batch_size, window, window, 3), dtype='float')
            for idx, sample in enumerate(range(batch_sample_start, batch_sample_end)):
                y_start = (sample // w_samples) * step
                y_end = y_start + window
                x_start = (sample % w_samples) * step
                x_end = x_start + window
                yieldme[idx] = arrinput[y_start:y_end,x_start:x_end].reshape(window, window, 3)
            yield yieldme
    def _make_rem(self, arrinput, dat, window, step=1, batch_size=32):
        last_batch = np.zeros((dat['remainder'], window, window, 3), dtype='float')
        sample_start = sum(dat['remainder_start'])
        sample_end = sample_start + (dat['remainder'] * step)
        for idx, sample in enumerate(range(sample_start, sample_end, step)):
            x_start = sample // dat['h_samples']
            x_end = x_start + window
            y_start = sample % dat['h_samples']
            y_end = y_start + window
            last_batch[idx] = arrinput[y_start:y_end,x_start:x_end].reshape(window, window, 3)
        return last_batch
    def dat_gen_rem(self, arrinput, window, step=1, batch_size=32):
        dat = self._make_dat(arrinput, window, step, batch_size)
        gen = self._predict_generator(arrinput, dat['samples'],
                dat['w_samples'], window, step, batch_size)
        rem = self._make_rem(arrinput, dat, window, step, batch_size)
        return dat, gen, rem
class AbstractModel:
    def __init__(self, generator, step=4, window_size=40, batch_size=32):
        self._generator = generator
        self.trainable = False
        self.step = step
        self.window_size = window_size
        self.batch_size = batch_size
        self.total_epochs = None
        self.total_batches = None
        self._tracker = tracker.SummaryTracker()
    def train(self, epochs=1, epoch_callback=lambda: None, weights=None):
        if len(self._generator) == 0:
            print("Generator is empty, so no training")
            return
        if not (hasattr(self, 'model') and self.model):
            raise Exception("Model needs to be created before it can be trained!")
        else:
            cb = callbacks.FitCallback(epoch_callback)
            self.total_epochs = epochs
            self.total_batches = len(self._generator)
            kwargs = {}
            if weights:
                kwargs['class_weight'] = {idx: val for idx, val in enumerate(weights)}
            self.model.fit(self._generator, epochs=epochs, callbacks=[cb], **kwargs)
        gc.collect()
        #print(self.__class__.__name__ + " " +
        #        str(multiprocessing.current_process().pid) + " summary:\n" +
        #        "\n".join(self._tracker.format_diff()))
    def transform_untranscribed(self):
        tut = self._generator.transform_untranscribed()
        gc.collect()
        #print(self.__class__.__name__ + " " +
        #        str(multiprocessing.current_process().pid) + " summary:\n" +
        #        "\n".join(self._tracker.format_diff()))
        return tut
    def _make_predictor_items(self, ndf=0, npd=0):
        return PredictorItems(ndf=ndf, npd=0)
    def predict(self, staff_ids, predictors, pd_items, step=None,
            despeckle=5, batch_info=tuple()):
        candr_log("predict", instance=self)
        # Make a consumer to be run on the main thread to grab staff data to be
        # predicted and run the prediction process
        pd_queue_consumer = AbstractPredictorDuplexQueueConsumer(pd_items.pd_queue,
                self.model, self._generator.window_size,
                len(self._generator.idxs), step, self.batch_size)
        # Make a consumer to be run on a different process to grab staff ids to
        # be fetched from the dataframe and return staff data
        df_queue_consumer = dataframe.DataFrameDuplexQueueConsumer(pd_items.df_queue,
                self._generator._stave_dataframe,
                label="Predict dataframe queue", use_lock=False)
        # Everything in this class goes to another thread
        predict_manager = AbstractPredictManager(staff_ids, pd_items,
                poison_queues=[pd_items.df_queue, pd_items.pd_queue], step=step,
                despeckle=despeckle, batch_info=batch_info,
                n_predictors=len(predictors))
        df_queue_consumer.start()
        # Child processes aren't allowed to start processes, so start the
        # processes here. They will wait for the event to be triggered before
        # they begin
        _ = [p.start() for p in predictors]
        predict_manager.start()
        # The queue consumer is the one that contains the single-threaded
        # predict() code, so this must be run on the main process. Note that
        # here we call run() rather than start() to keep it on the main process.
        pd_queue_consumer.run()
        # The predict_manager is forked to set up everything else and to provide
        # the stdout for tqdm etc
        predict_manager.join()
        _ = [p.join() for p in tqdm(predictors, desc="Waiting for predictor threads to finish")]
        df_queue_consumer.join()
        res = pd_items.get_result()
        del pd_queue_consumer
        del df_queue_consumer
        del predict_manager
        for p in predictors:
            del p
        pd_items.shutdown_manager()
        del pd_items
        gc.collect()
        candr_log("Predicted:", res, instance=self)
        #print(self.__class__.__name__ + " " +
        #        str(multiprocessing.current_process().pid) + " summary:\n" +
        #        "\n".join(self._tracker.format_diff()))
        return res
class PredictorItems():
    def __init__(self, ndf=0, npd=0):
        self._manager = multiprocessing.Manager()
        self.df_queue = duplex_queue.DuplexQueue(self._manager, size=ndf)
        self.pd_queue = duplex_queue.DuplexQueue(self._manager, size=npd)
        self.processing_queue = duplex_queue.InOutQueue(self._manager)
        self.out_queue = self._manager.Queue()
        self.out_queue_lock = self._manager.Lock()
        self.start_stop_event = self._manager.Event()
    def shutdown_manager(self):
        if hasattr(self, '_manager'):
            self._manager.shutdown()
            del self._manager
    def put_result(self, val):
        candr_log("Put result", val, instance=self)
        with self.out_queue_lock:
            candr_log("out_queue lock acquired", instance=self)
            self.out_queue.put(val)
            candr_log("approx " + str(self.out_queue.qsize()) + " results processed", instance=self)
    def get_result(self, timeout=5):
        candr_log("Getting result", instance=self)
        results = {}
        with self.out_queue_lock:
            candr_log("out_queue lock acquired", instance=self)
            candr_log("approx " + str(self.out_queue.qsize()) + " results to get", instance=self)
            while True:
                ret = None
                try:
                    ret = self.out_queue.get(timeout=timeout)
                except queue.Empty:
                    break
                assert isinstance(ret, tuple) == True
                assert len(ret) == 2
                results[ret[0]] = ret[1]
        candr_log("get() timed out after " + str(timeout) + " seconds: queue is empty. Retrieved " + str(len(results)) + " results", instance=self)
        return results
    def trigger_event(self):
        candr_log("Triggering event", instance=self)
        self.start_stop_event.clear()
        self.start_stop_event.set()
    def fill_processing_queue(self, ids):
        return self.processing_queue.fill_in(ids)
class AbstractPredictManager(multiprocessing.Process):
    def __init__(self, staff_ids, pd_items, poison_queues=[], step=None,
            despeckle=5, batch_info=tuple(), n_predictors=1):
        super(AbstractPredictManager, self).__init__()
        self._staff_ids = staff_ids
        self._pd_items = pd_items
        self._poison_queues = poison_queues
        self._step = step
        self._batch_info = batch_info
        self._despeckle = despeckle
        self._n_predictors = n_predictors
    def _trigger_event(self):
        self._pd_items.trigger_event()
    def run(self):
        candr_log("run", instance=self)
        # Add staff ids to the processing queue
        self._pd_items.fill_processing_queue(self._staff_ids)
        candr_log("starting predictors", instance=self)
        # Triggering the event starts the predictions in the other processes
        self._trigger_event()
        # Generator to get predictions from queue until it is poisoned
        def prog_gen(p_q, n):
            for i in range(n):
                candr_log("prog_gen getting", instance=self)
                n_o = p_q.next_out()
                while n_o is not None:
                    candr_log("prog_gen got", instance=self)
                    yield n_o
                    candr_log("prog_gen getting", instance=self)
                    n_o = p_q.next_out()
                candr_log(str(i+1) + "/" + str(n) + " predictors have ended", instance=self)
            candr_log("prog_gen ending. All predictors have ended", instance=self)
            return
        # Show progress bar while making predictions into queue
        desc = "Predicting"
        if len(self._batch_info) == 2:
            desc += " (" + str(self._batch_info[0]) + "/" + str(self._batch_info[1]) + ")"
        for ret in tqdm(prog_gen(self._pd_items.processing_queue,
                self._n_predictors), total=len(self._staff_ids), desc=desc):
            candr_log("Received prediction for " + str(ret[0]), ret[1], instance=self)
            self._pd_items.put_result(ret)
        candr_log("poisoning queues", instance=self)
        for q in self._poison_queues:
            q.poison()
        # Triggering the event again should stop all the other processes
        self._trigger_event()
        return
class AbstractPredictor(multiprocessing.Process):
    def __init__(self, step, window_size, batch_size, scaling, pd_items,
            pred_step=1, pred_despeckle=None):
        super().__init__()
        self.step = step
        self.pred_step = pred_step
        self.window_size = window_size
        self.batch_size = batch_size
        self.df_getter = duplex_queue.DuplexQueueGetter(pd_items.df_queue)
        self.pd_getter = duplex_queue.DuplexQueueGetter(pd_items.pd_queue)
        self.processing_queue = pd_items.processing_queue
        self.start_stop_event = pd_items.start_stop_event
        self.scaling = scaling
        self._tracker = tracker.SummaryTracker()
        self.pred_despeckle = pred_despeckle
    def _unscale_and_shift(self, staff_id, px, py):
        scale = self.scaling[staff_id]
        candr_log("_unscale_and_shift(): " + str(staff_id) + " scaling is", scale, instance=self)
        (outxscale, sx), (outyscale, sy) = scale
        return ((px / outxscale) - sx, (py / outyscale) - sy)
    def _unscale(self, staff_id, px, py):
        scale = self.scaling[staff_id]
        (outxscale, sx), (outyscale, sy) = scale
        return (px / outxscale, py / outyscale)
    def do_prediction(self, staff_dat):
        candr_log("do_prediction", instance=self)
        return self.pd_getter.get(staff_dat)
    def get_staff_data(self, staff_id, required_h=None):
        candr_log("get_staff_data", instance=self)
        data = self.df_getter.get(staff_id)
        if data is None:
            return None
        if not required_h:
            return data
        diff_h = required_h - data.shape[0]
        hdh = diff_h // 2
        # our image is too tall
        if diff_h < 0:
            data = data[-hdh:(diff_h - hdh),:]
        # our image is too squat, make a new array that is the right height and
        # place our sample in the center
        elif diff_h > 0:
            new_data = np.zeros((required_h, data.shape[1]), dtype=data.dtype)
            new_data[hdh:(hdh - diff_h),:] = data
            data = new_data
        # Our image should be the right size now
        assert data.shape[0] == required_h
        return data
    def predict(self, staff_id):
        candr_log("predict", instance=self)
        return staff_id
    def run(self):
        candr_log("run. Waiting for event", instance=self)
        self.start_stop_event.wait()
        candr_log("received event, processing!", instance=self)
        # Poison the queue so that once everything is processed, we quit
        self.processing_queue.poison_in()
        task = self.processing_queue.next_in()
        while task:
            candr_log("received job", instance=self)
            self.processing_queue.set_out((task, self.predict(task)))
            gc.collect()
            #print(self.__class__.__name__ + " " +
            #        str(multiprocessing.current_process().pid) + " summary:\n" +
            #        "\n".join(self._tracker.format_diff()))
            task = self.processing_queue.next_in()
        self.processing_queue.poison_out()
        candr_log("no more jobs. Waiting for event", instance=self)
        self.start_stop_event.wait()
        candr_log("received event, ending!", instance=self)
