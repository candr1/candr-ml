from tensorflow import keras
from . import abstract
from ..generators import staffline
from ..misc import image
from ..misc.candr_log import candr_log
import numpy as np
import json
from scipy.ndimage import median_filter, label
import psutil

class StafflineModel(abstract.AbstractModel):
    def __init__(self, pg_conn, bucket, facsimile_set_id, out_w=1000, mul=0.02,
            window_size=20, batch_size=32, step=4, staffline_w=5, shuffle=True):
        gen = staffline.StafflineGenerator(pg_conn, bucket, facsimile_set_id,
                out_w=out_w, mul=mul, window_size=window_size,
                batch_size=batch_size, step=step, shuffle=shuffle,
                staffline_w=staffline_w)
        super(StafflineModel, self).__init__(gen, step, window_size, batch_size)
        if self._generator.n_records > 0:
            input_shape = (self._generator.window_size, self._generator.window_size, 3)
            self.model = keras.models.Sequential()
            self.model.add(keras.layers.Conv2D(32, 3, activation='relu', input_shape=input_shape))
            self.model.add(keras.layers.MaxPooling2D((2, 2)))
            self.model.add(keras.layers.Conv2D(32, 3, activation='relu', input_shape=input_shape))
            self.model.add(keras.layers.MaxPooling2D((2, 2)))
            self.model.add(keras.layers.Flatten())
            self.model.add(keras.layers.Dense(256, activation='relu'))
            self.model.add(keras.layers.Dense(len(self._generator.idxs), activation='sigmoid'))
            optim = keras.optimizers.Adam(learning_rate=0.0001)
            self.model.compile(optimizer=optim, loss='categorical_crossentropy', metrics=['accuracy'])
            self.model.summary()
            self.trainable = True
    def train(self, epochs=1, epoch_callback=lambda: None):
        if self.trainable:
            ratio = self._generator.sample_ratio
            total = sum(ratio)
            weights = [(0 if i == 0 else ((1.0 / i)*total)) for i in ratio]
            print("Weights:", json.dumps(weights))
            super().train(epochs, epoch_callback, weights=weights)
    def predict(self, staff_ids, step=None, despeckle=5, batch_info=tuple()):
        cpus = psutil.cpu_count()
        pd_items = self._make_predictor_items(ndf=cpus, npd=1)
        pred_kwargs = {}
        super_kwargs = {
            "batch_info": batch_info
        }
        if step:
            pred_kwargs['pred_step'] = step
            super_kwargs['step'] = step
        if despeckle:
            pred_kwargs['pred_despeckle'] = despeckle
            super_kwargs['despeckle'] = despeckle
        predictors = [StafflinePredictor(self._generator.out_w, self.step,
                self.window_size, self.batch_size, self._generator.scaling,
                pd_items, **pred_kwargs) for i in range(cpus)]
        return super(StafflineModel, self).predict(staff_ids, predictors,
                pd_items, **super_kwargs)
class StafflinePredictor(abstract.AbstractPredictor):
    def __init__(self, out_w, step, window_size, batch_size, scaling, pd_items,
            pred_step=1, pred_despeckle=None):
        self.out_w = out_w
        if not pred_despeckle:
            pred_despeckle = 5
        super(StafflinePredictor, self).__init__(step, window_size, batch_size,
                scaling, pd_items, pred_step, pred_despeckle)
    def predict(self, staff_id):
        staff_dat = self.get_staff_data(staff_id)
        this_step = self.pred_step if self.pred_step is not None else self.step
        predict_out = self.do_prediction(staff_dat)[:,:,0]
        candr_log("Prediction shape", list(predict_out.shape), instance=self)
        heatmap = np.zeros((staff_dat.shape[0], staff_dat.shape[1]), dtype='float')
        del staff_dat
        half_window = self.window_size // 2
        quart_window = half_window // 2
        radial_x = np.linspace(-1, 1, half_window)[:, None]
        radial_y = np.linspace(-1, 1, half_window)[None, :]
        radial = np.sqrt(radial_x ** 2 + radial_y ** 2)
        radial = np.max(radial) - radial
        radial = (radial - np.min(radial))/np.ptp(radial)
        for idx, sample in np.ndenumerate(predict_out):
            #y_start = idx[0] * this_step + quart_window
            #y_end = y_start + half_window
            #x_start = idx[1] * this_step + quart_window
            #x_end = x_start + half_window
            #heatmap[y_start:y_end,x_start:x_end] += sample * radial
            y_start = idx[0] * this_step + half_window
            y_end = (idx[0] + 1) * this_step + half_window
            x_start = idx[1] * this_step + half_window
            x_end = (idx[1] + 1) * this_step + half_window
            heatmap[y_start:y_end,x_start:x_end] += predict_out[idx[0], idx[1]]
        normalized = 255 * (heatmap - np.min(heatmap))/np.ptp(heatmap)
        del heatmap
        #image.export_image(normalized, "stafflines-heatmap-" + str(staff_id) + ".png")
        # Threshold heatmap
        otsu = image.otsu(median_filter(normalized, size=self.pred_despeckle)).astype('uint8')
        del normalized
        #image.export_image(otsu, "stafflines-mask-" + str(staff_id) + ".png")
        # Label the features extracted
        labels, nlabels = label(otsu)
        # Lines should be at least half the width of the stave, that's
        # reasonable, right?
        min_line_length = self.out_w // 2
        lines = []
        # For every feature
        for i in range(1, nlabels + 1):
            # Get an array of coordinates of that feature
            coords = np.array(np.where(labels==i)).T
            xcoords = coords[:,1]
            # Find the coords with the minimum and maximum x-coordinate
            minc = coords[np.argmin(xcoords)]
            maxc = coords[np.argmax(xcoords)]
            # Calculate the length of that line
            line_len = np.linalg.norm(maxc - minc)
            if line_len < min_line_length:
                candr_log("Found segment with length " + str(int(line_len))
                    + " but minimum line length was " +
                    str(int(min_line_length)), instance=self)
                continue
            # Swap x and y, convert to list
            a = minc[[1,0]].tolist()
            n_a = self._unscale_and_shift(staff_id, a[0], a[1])
            b = maxc[[1,0]].tolist()
            n_b = self._unscale_and_shift(staff_id, b[0], b[1])
            candr_log("Predicted " + str((a, b)) + " which was unscaled into " + str((n_a, n_b)), instance=self)
            lines.append((n_a, n_b))
            del coords
        return lines
