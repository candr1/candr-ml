import os
import sys
from datetime import datetime
import json
import psycopg2
from itertools import zip_longest, islice
from candrml.models import staffline, item
from candrml.misc import image, status
from candrml.misc.candr_log import candr_log
from tqdm import tqdm

def grouper(n, iterable, padvalue=None):
    return zip_longest(*[iter(iterable)]*n, fillvalue=padvalue)

status_host = os.getenv('CANDR_STATUS_HOST', default='www.candr.tk/machinelearning/').encode('utf-8').decode('unicode_escape')
private_key = os.getenv('CANDR_STATUS_PRIVATE_KEY').encode('utf-8').decode('unicode_escape')
transaction_key_password = os.getenv('CANDR_STATUS_KEY_PASSWORD', default='').encode('utf-8').decode('unicode_escape')
postgres_host = os.getenv('CANDR_POSTGRES_HOST', default='localhost').encode('utf-8').decode('unicode_escape')
postgres_port = int(os.getenv('CANDR_POSTGRES_PORT', default='5432').encode('utf-8').decode('unicode_escape'))
postgres_user = os.getenv('CANDR_POSTGRES_USER', default='candr').encode('utf-8').decode('unicode_escape')
postgres_password = os.getenv('CANDR_POSTGRES_PASS').encode('utf-8').decode('unicode_escape')
postgres_database = os.getenv('CANDR_POSTGRES_DATABASE', default='candr').encode('utf-8').decode('unicode_escape')
batch_size = int(os.getenv('CANDR_BATCHSIZE', default='32').encode('utf-8').decode('unicode_escape'))
env_shuffle = os.getenv('CANDR_SHUFFLE').encode('utf-8').decode('unicode_escape')
shuffle = False if env_shuffle and (env_shuffle.lower() not in ['true', 't', '1']) else True
out_w = int(os.getenv('CANDR_OUTW', default='1000').encode('utf-8').decode('unicode_escape'))
mul = float(os.getenv('CANDR_MUL', default='0.02').encode('utf-8').decode('unicode_escape'))
step = int(os.getenv('CANDR_STEP', default='4').encode('utf-8').decode('unicode_escape'))
image_api = os.getenv('CANDR_IMAGE_API', default='www.candr.tk/imagedl/').encode('utf-8').decode('unicode_escape')
gpg_home = os.getenv('GNUPGHOME', default='/root').encode('utf-8').decode('unicode_escape')
image_cache = os.getenv('CANDR_IMAGE_CACHE').encode('utf-8').decode('unicode_escape')
output_json = os.getenv('CANDR_OUTPUT_JSON', default='/tmp/candr-output.json').encode('utf-8').decode('unicode_escape')
predict_batch = int(os.getenv('CANDR_BATCH_PREDICT', default='1000').encode('utf-8').decode('unicode_escape'))
max_predict = int(os.getenv('CANDR_MAX_PREDICT', default='65535').encode('utf-8').decode('unicode_escape'))

ml_details = {
    "batch_size": batch_size,
    "shuffle": shuffle,
    "out_w": out_w,
    "mul": mul,
    "step": step
}
postgres_secrets = {
    "host": postgres_host,
    "username": postgres_user,
    "password": postgres_password,
    "database": postgres_database,
    "port": postgres_port
}

su = status.StatusUpdater(status_host, private_key, transaction_key_password,
        postgres_secrets, gpg_home, dummy=True)
ci = image.CloudImage(image_api, image_cache)

models = [
    {"name": "Stafflines", "class": staffline.StafflineModel},
    {"name": "Items", "class": item.ItemModel}
]
su.update_status_async({
    "stage": "launched"
})
print("Training:", [m['name'] for m in models])
print("Details:", ml_details)
def open_conn(postgres_secrets):
    c = psycopg2.connect(host=postgres_secrets['host'],
            port=postgres_secrets['port'], user=postgres_secrets['username'],
            password=postgres_secrets['password'],
            database=postgres_secrets['database'])
    c.autocommit = True
    return c
try:
    conn = open_conn(postgres_secrets)
    cur = conn.cursor()
    cur.execute('''SELECT id, name FROM facsimile_sets WHERE deleted_at IS NULL;''')
    facsimile_sets = list(cur.fetchall())
    conn.close()
    final_info = {}
    for facsimile_set_id, facsimile_set_name in facsimile_sets:
        staff_info = {}
        for model in models:
            conn = open_conn(postgres_secrets)
            cur = conn.cursor()
            name = model['name']
            cls = model['class']
            su.update_status_async({
                "stage": "train",
                "model": name,
                "facsimile_set_name": facsimile_set_name
            })
            instance = cls(conn, ci, facsimile_set_id, **ml_details)
            total_epochs = instance.total_epochs
            def update_callback(epoch):
                su.update_status_async({
                    "stage": "train",
                    "model": name,
                    "facsimile_set_name": facsimile_set_name,
                    "epoch": (epoch + 1),
                    "total_epochs": total_epochs,
                })
            instance.train(epoch_callback=update_callback)
            su.update_status_async({
                "stage": "transform",
                "model": name,
                "facsimile_set_name": facsimile_set_name
            })
            instance.transform_untranscribed()
            su.update_status_async({
                "stage": "predict",
                "model": name,
                "facsimile_set_name": facsimile_set_name
            })
            untranscribed_staves = list(grouper(predict_batch, islice(instance._generator._untranscribed_staves, max_predict)))
            for idx, group in enumerate(untranscribed_staves):
                b_i = (idx + 1, len(untranscribed_staves))
                su.update_status_async({
                    "stage": "predict",
                    "batch": b_i[0],
                    "total_batches": b_i[1],
                    "model": name,
                    "facsimile_set_name": facsimile_set_name
                })
                pred_ret = instance.predict(list(group), batch_info=b_i, step=1)
                class_name = instance.__class__.__name__
                for sid, pred in pred_ret.items():
                    if sid not in staff_info:
                        staff_info[sid] = {}
                    staff_info[sid][class_name] = pred
            conn.close()
        def make_stave_record(info):
            c_clefs = info['ItemModel']['c_clefs'] if 'c_clefs' in info['ItemModel'] else []
            f_clefs = info['ItemModel']['f_clefs'] if 'f_clefs' in info['ItemModel'] else []
            sharps = info['ItemModel']['sharps'] if 'sharps' in info['ItemModel'] else []
            flats = info['ItemModel']['flats'] if 'flats' in info['ItemModel'] else []
            annot_c_clef = [{**rec, 'type': 'C'} for rec in c_clefs]
            annot_f_clef = [{**rec, 'type': 'F'} for rec in f_clefs]
            all_clefs = annot_c_clef + annot_f_clef
            annot_sharp = [{**rec, 'type': 'Sharp'} for rec in sharps]
            annot_flat = [{**rec, 'type': 'Flat'} for rec in flats]
            all_accidentals = annot_sharp + annot_flat
            return json.dumps({
                "stafflines": info['StafflineModel'],
                "divisiones": info['ItemModel']['divisiones'],
                "notes": info['ItemModel']['notes'],
                "clefs": all_clefs,
                "accidentals": all_accidentals
            })
        for sid, info in staff_info.items():
            final_info[sid] = make_stave_record(info)
    su.update_status_async({
        "stage": "finalise"
    })
    try:
        jfp = open(output_json, 'w+')
        json.dump(final_info, jfp)
        jfp.close()
    except Exception as e:
        print("Error saving JSON: {0}".format(e))
    now = datetime.now()
    conn = open_conn(postgres_secrets)
    cur = conn.cursor()
    for sid, info in tqdm(final_info.items(), desc="Adding predictions to database"):
        cur.execute('''UPDATE staves SET predictions = %s, last_predicted = %s WHERE id = %s;''',
            (info, now, sid))
    su.update_status_async({
        "stage": "done"
    })
    conn.close()
except:
    su.update_status_async({
        "stage": "error"
    })
    conn.close()
    raise
